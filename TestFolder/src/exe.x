#!/bin/bash

filename1='../BigData/BigData.dat'
filename2='../write/fit_Numbers.txt'
filename3='../write/fittingdata'

echo Start

[ -e $filename2 ] && rm -- $filename2
#rm -r $filename2
touch $filename2
[ -e $filename3 ] && rm -- $filename3
#rm -r $filename3
touch $filename3

for ii in {0..4}
do
let jj=$ii*9
let aa=1+$jj
let bb=9+$jj
echo $aa,$bb
echo >../TestData.txt

for ((i=$aa;i<=$bb;i++))
do
p=($(sed -n "${i}p" $filename1))
echo $p
echo $p>>../TestData.txt
done < $filename1
cd ../../
echo $(pwd)
#sed -i 'pre-program.txt' 1d pre-program.txt
perl -ni -e 'print unless $. == 1' pre-program.txt
echo '1' | cat - pre-program.txt > temp && mv temp pre-program.txt
gcc -std=c99 -o start start.c -lm
./start
gcc -std=c99 -g LBM.c input.c -lm
./a.out
#echo $(pwd)
#read
gnuplot ./TestFolder/src/fitplot.l
#read
echo EndFor $ii
cd ./TestFolder/src
echo $(pwd)
#read

done

#Test the accuracy&decide to stop/continue
#=========================================
#===============================================
#=====================================================
filename1='../write/fit_Numbers.txt'
filename2='../write/analytics.t'

echo Start the testComparison
sum=0
for ((i=1;i<=5;i++))
do
p=($(sed -n "${i}p" $filename1))
g=($(sed -n "${i}p" $filename2))
echo Numercal /n Analytical
echo $p
echo $g
h=$(awk -v m=$p -v mm=$g 'BEGIN {printf "%3.28e",m/mm-mm/mm }')
calc=$(awk -v m=$h 'BEGIN {printf "%3.28e",sqrt(m*m) }')

echo Absolute Num-Analy/Analy:
echo $calc

sum=$(awk -v m=$sum -v mm=$calc 'BEGIN {printf "%3.28e",m+mm }')
echo sum of error is:
echo $sum
echo at test number:
echo $i
done
echo toal sum of errors
echo $sum
errorTol=1.04e-4
echo which should be smaller than $errorTol

echo =====================
echo =====================
echo =====================
echo $(pwd)
cp ../write/fit_Numbers.txt ../../../../Test/PoiseuilleForce/data/fit_Numbers.txt
cd ../../../../Test/PoiseuilleForce
echo $(pwd)
paste ./data/Tau.t ./data/fit_Numbers.txt > ./data/numerical.dat
paste ./data/Tau.t ./data/analytics.t > ./data/analytical.dat
gnuplot ./script/plot.gnu
awk -v m=$sum -v mm=$errorTol 'BEGIN {
if (m >=mm)
{
print "Test is unsucsessful due to large error"
print "Note: The previous sum error, tested locally was 1.0304907884983727228700445266e-04"
print "Sum of error is:", m, ">=", mm
print "Exiting the operations with -exit 1- "
exit 1
}
else if (m<mm)
{
print "Test is successful"
print "Sum of error is:", m, "<",mm
}
}'

