set fit logfile "./TestFolder/write/fittingdata"
set print "./TestFolder/write/fit_Numbers.txt" append
f(x) = a + b*x + c*x**2
fit [0:1] [0:1.000000e+00] f(x) "./output/Out_GraphY.dat" using 1:2 via a,b,c
print a

