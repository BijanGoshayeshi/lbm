//******************************************************************************
//Title:
//        Lattice Boltzmann methods (LBM) program for two-dimensional flows
//Version:
//        Version 2.01.03
//History:
//        Written by Bijan Gosaheyshi, 9/2018.
//******************************************************************************
/* 
Documentation
Imortant parameters:
A) The solver will be difined by preprocessors such as couette, pois_press, etc
B) The euilibrium function can be switched between linear and square by EquilFunc

C)Topics on the compile
C-0)make zero values
C-1)geometry setting, problem definition, analytical values
C-2)force setting
C-3)initialization: density, vlocity, 
    distribution: equilibrium, force, populations
C-4)Main algorithm
 C-4-1)Macroscopic evaluation
 C-4-2)Check convergence and if possible finish the simulation
 C-4-3)Compute equilibrium distribution

 {
 C-4-5)Compute forcing term
 C-4-6)Collision step
 C-4-7)Boundary condition (inlet and outlet e.g. pressure gradiant)
 }

 {
 C-4-8)Streaming step
 C-4-9)Boundary condition wall interactions
 }
--------------------------------------------------------------------------------------------------------------------------------*/
#include "inc1.h"

int main()
{
    //float var = M_PI*M_PI;
    //long double var2 = M_PI*M_PI;
    //long double var3 = 3.14159265358979323846*3.14159265358979323846;
    ////printf("\n normal:%f\n sci:%e \n or \n sci:%E   \n",var,var,var);
    //printf("\n normal:%Lf\n sci:%Le \n or \n sci:%LE   \n",var3,var3,var3);
    //printf("\n normal:%Lf\n sci:%Le \n or \n sci:%LE   \n",var2,var2,var2);
    //getchar();
    initialization();
    for (t = 1; t < NSTEPS + 1; t++)
    { /////Main Algorithm/////
        Macroscopic_Evaluation();
        CheckConvergence();
        EquilibriumCalculation();
        collision();
        BoundaryCheck();
        Stream_and_WallReflection();
        Output_Write(WriteItermode, output_interval, eachstep_interval);
        Check_Finish();
    } /////Main Algorithm/////
    return 0;
}