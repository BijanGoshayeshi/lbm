# Project Title

An implementation of the **LBM** ( Lattice Boltzmann Methods) numerical tool in the C programming language for two-dimensional problems based on the D2Q9 and the D2Q21 velocity set models.


For more informaton, please contact me with "Bijan.Goshayeshi@gmail.com".

