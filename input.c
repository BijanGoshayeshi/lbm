//******************************************************************************
//Title:
//        Lattice Boltzmann methods (LBM) program for two-dimensional flows
//Version:
//        Version 2.01.03
//History:
//        Written by Bijan Gosaheyshi, 9/2018.
//******************************************************************************

/*The list of functions:
void finish_The_Simulation();
void write_Kolmo();
void writeLongIteration();
void writeEachStep();
void writeEachStepDiag();
void L2error();
void collision();
void EquilibriumCalculation();
void initialization();
void Macroscopic_Evaluation();
void CheckConvergence();
void BoundaryCheck();
void Stream_and_WallReflection();
void Output_Write();
void Check_Finish();
void U_analyCalculation ();
void write();
*/

#include "inc1.h"

////////////////////////////////////////////////////////////////////
//function to write L2 and finish simulation
void finish_The_Simulation()
{
  U_analyCalculation(1);

  L2error();
  printf("\nIteration = %d \n", t);
  printf("Accuracy information...\n");
  printf("L2= %Le \n", L2);
#if (defined forced_kolmg || defined decay_kolmg)
  printf("L2Y= %Le \n", L2Y);
#endif
  printf("conv= %Le \n", conv);
  printf("@finish the simulation\n");

  write(4); //"Out_Contour.dat" //"Out_GraphY.dat" //"Out_GraphX.dat"
#ifdef diagDecay
  write(5);
#endif

  /////write for execution after simulation finihed
  FILE *fp;
  fp = fopen("./script/execute1.x", "w");
  fprintf(fp, "#! /bin/bash \n");
  fprintf(fp, "cd .. \n");
  fprintf(fp, "cd output/ \n");
  fprintf(fp, "\ngnuplot plotEachstep%d.l \n", En);
  fclose(fp); //Graph
  /////write for execution after simulation finihed
  fp = fopen("./output/fitplot.l", "w");
  fprintf(fp, "set fit logfile \"fittingdata\" \n");
  fprintf(fp, "f(x) = a + b*x + c*x**2 \n");
  fprintf(fp, "fit [0:%d] [0:%Le] f(x) \"Out_GraphY.dat\" using 1:2 via a,b,c \n", NY / NY, u_max / u_max);
  fprintf(fp, "set xrange[0:%d] \n", NY / NY);
  fprintf(fp, "set yrange[0:%Le] \n", u_max / u_max);
  fprintf(fp, "plot \"Out_GraphY.dat\" using 1:2, f(x) \n");
  fprintf(fp, "pause -1 \"Hit any key to continue\" \n");
  fclose(fp); //Graph
              //finish and exit

  exit(0);
}

//function to allocate 3D array
long double ***AllocateArray3D(int dim1, int dim2, int dim3)
{

  int i, j, k;

  long double ***p = (long double ***)malloc(dim1 * sizeof(long double **));

  for (i = 0; i < dim1; i++)
  {
    p[i] = (long double **)malloc(dim2 * sizeof(long double *));
    for (j = 0; j < dim2; j++)
      p[i][j] = (long double *)malloc(dim3 * sizeof(long double));
  }

  return p;
}

//function to allocate 2D array

long double **AllocateArray2D(int dim1, int dim2)
{

  int i, j;

  long double **p = (long double **)malloc(dim1 * sizeof(long double *));

  for (i = 0; i < dim1; i++)
    p[i] = (long double *)malloc(dim2 * sizeof(long double));

  return p;
}

////////////////////////////////////////////////////////////////////
//function to write for kolmogorov flow
void write_Kolmo()
{
  if (iteration > 0 && iteration < WriteIter + 1)
  {

    FILE *fp;
    char buf[65];
    sprintf(buf, "./output/kol/Kol%d.dat", iteration);
    fp = fopen(buf, "w");

#ifdef sound_waves
    char strr[] = "   \"X\", \"Density-perturbed/dens_max\" , \"u/(Csound*dens_max)\" , \"v\", \"Energy\" ";
#elif defined(Doubly_periodic_shear_layers)
    char strr[] = "   \"X\", \"Y\" , \"u\" , \"v\", \"vorticity\", \"DivU\" ";
#else
    char strr[] = "   \"X\", \"U\" ";
#endif

    fprintf(fp, "VARIABLES = %s\n", strr);
    fprintf(fp, "ZONE T= \"time %d\", I=%d, J=%d%s \n", iteration, NX, NY, ", F= POINT");
    //1D domain write
    for (int j = 0; j < NX; j++)
    {
      int kk = NY / 2;

//
#ifdef decay_kolmg
      fprintf(fp, "%Le %Le %Le\n", x[j], v[j][kk], rho[j][kk] - 1.0000);
#elif defined(forced_kolmg)
      fprintf(fp, "%Le %Le\n", x[j], v[j][kk]);
#elif (defined pois_press || defined pois_PrFr)
      fprintf(fp, "%Le %Le\n", x[j], u[j][kk - 1]);
#elif defined couette
      fprintf(fp, "%Le %Le\n", x[j], u[j][NY - 1]);
#elif defined sound_waves
      a11 = u[j][kk] * u[j][kk] + RT * (rho[j][kk] - 1.0000) * (rho[j][kk] - 1.0000);
      fprintf(fp, "%Le %Le %Le %Le %Le\n", x[j], (rho[j][kk] - 1.0000) / dens_max, u[j][kk] / (c_sound * dens_max), v[j][kk], a11);
#endif
      //
    }
///2D domain write
//
#ifdef Doubly_periodic_shear_layers
    fprintf(fp, "  \n");
    for (int j = 0; j < NY; j++)
    {
      for (int i = 0; i < NX; i++)
      {
        fprintf(fp, "%Le %Le %Le %Le %Le %Le\n", x[i] / NX, y[j] / NY, u[i][j], v[i][j], vorticity[i][j], DivU[i][j]);
      }
      fprintf(fp, "  \n");
    } //
#endif
    fclose(fp);

    if ((iteration == 1) && (finisher == 0))
    {
      FILE *gp;
      gp = fopen("./output/pKolm.l", "w");
#ifdef sound_waves

      fprintf(gp, " reset \n ");
      fprintf(gp, "set terminal postscript eps enhanced color font 'Helvetica,20' \n");
      fprintf(gp, "set output 'kol/figs/pKolm.eps'\n");

      fprintf(gp, "set title \"Length vs. density\" \n");
      fprintf(gp, "set ylabel \"Density-perturbed\" \n");
      fprintf(gp, "set xlabel \"Length-X\" \n");
      fprintf(gp, "plot \"kol/Kol%d.dat\" using 1:2 title '1' with lines,", iteration);
#elif (defined Doubly_periodic_shear_layers)
      fprintf(gp, "#nothing to do in Doubly_periodic_shear_layers \n ");
#else
      fprintf(gp, " reset \n ");
      fprintf(gp, "set title \"Length vs. Vvel\" \n");
      fprintf(gp, "set ylabel \"Vvel\" \n");
      fprintf(gp, "set xlabel \"Length-X\" \n");
      fprintf(gp, "plot \"kol/Kol%d.dat\" using 1:2 title '1' with lines,", iteration);
#endif

      fclose(gp);
    }

#ifdef Doubly_periodic_shear_layers
    u[1][1] = u[1][1]; //just do nothing
#else
    else if ((iteration > 1) && (iteration + WriteItermode <= WriteIter) && (finisher == 0))
    {
      FILE *gpp;
      gpp = fopen("./output/pKolm.l", "a+");
      fprintf(gpp, "\"kol/Kol%d.dat\" using 1:2 title '%d',", iteration, iteration);
      fclose(gpp);
    }
    else if ((iteration == WriteIter) || (iteration + WriteItermode > WriteIter) || (finisher == 1))
    {
      //finisher2=1;
      FILE *gppp;
      gppp = fopen("./output/pKolm.l", "a+");
      fprintf(gppp, "\"kol/Kol%d.dat\" using 1:2 title '%d' \n", iteration, iteration);
      fprintf(gppp, "pause -%d \"Hit any key to continue\" ", iteration);
      fclose(gppp);
      //printf ("getchar");
      //getchar();
    }
#endif
  }
}

void writeLongIteration()
{
  //L2error calculation
  L2error();
  //
  printf("\nIteration = %d \n", iteration);
  printf("Accuracy information...\n");
  printf("L2= %Le \n", L2);
#if (defined forced_kolmg || defined decay_kolmg)
  printf("L2Y= %Le \n", L2Y);
#endif
  printf("conv= %Le \n", conv);
  printf("Simulation continues...\n");

  //write graph and contour
  write(4);
}

////////////////////////////////////////////////////////////////////
void writeEachStep()
{
  //U_analyCalculation (1);
  L2error();
  write(4);

  FILE *hppp;
  char buf1[65];
  sprintf(buf1, "./output/writeEachStep%d.dat", En);
  if (iteration == 1)
  {
    hppp = fopen(buf1, "w");
  }
  else
  {
    hppp = fopen(buf1, "a+");
  }
  int alfa = NX / (2 * En);
  alfa = alfa - 1;
  int alfa2 = NY / (2 * En2);
  alfa2 = alfa2 - 1;
//
#if (defined decay_kolmg || defined sound_waves || defined Doubly_periodic_shear_layers)
  int beta = NY / 2;
  int beta2 = NX / 2;
#elif defined(forced_kolmg)
  int beta = NY / 2;
  int beta2 = NX / 2;
#elif (defined pois_press || defined pois_PrFr)
  int beta = NY / 2;
  int beta2 = NX / 2;
#elif defined couette
  int beta = NY - 1;
  int beta2 = NX / 2;
#endif

  //
  if (alfa <= 0)
    alfa = 1;
  if (alfa >= NX)
    alfa = NX - 1;
  if (alfa2 <= 0)
    alfa2 = 1;
  if (alfa2 >= NY)
    alfa2 = NY - 1;
  if (NY == 1)
  {
    alfa2 = 0;
    beta = 0;
  }
  if (NX == 1)
  {
    alfa = 0;
    beta2 = 0;
  }

  long double xalfa = x[alfa];
  long double yalfa = y[alfa2];
  long double inside1 = -(En * En) * (M_PI * M_PI) * nuo * iteration / (NX * NX);
  long double inside2 = (En)*M_PI * (xalfa) / (NX);
  long double inside12 = -(En2 * En2) * (M_PI * M_PI) * nuo * iteration / (NY * NY);
  long double inside22 = (En2)*M_PI * (yalfa) / (NY);
  //long double VDecayAnaltcl=u_max*exp(inside1)*sinl(inside2);

#if (defined decay_kolmg)
  long double VDecayAnaltcl = u_max * exp(inside1) * sinl(inside2);     // =f(t)
  long double VDecayAnaltcl2 = u_max2 * exp(inside12) * sinl(inside22); // =f(t)
  long double VDecayAnaltclTwo = u_analy[beta];
  long double VDecayAnaltclThree = u_analy[beta];
#elif (defined sound_waves || defined Doubly_periodic_shear_layers)
  long double VDecayAnaltcl = u[alfa][beta];  // =f(t)
  long double VDecayAnaltcl2 = v[alfa][beta]; // =f(t)
  long double VDecayAnaltclTwo = 1.0;
  long double VDecayAnaltclThree = 1.0;
  a11 = u[alfa][beta] * u[alfa][beta] + RT * (rho[alfa][beta] - 1.0000) * (rho[alfa][beta] - 1.0000);
#elif defined(forced_kolmg)
  long double VDecayAnaltcl = u_max - (u_max * exp(inside1) * sinl(inside2));
  long double VDecayAnaltcl2 = VDecayAnaltcl;
  // = Umax-f(t)
  long double VDecayAnaltclTwo = u_analy[beta];
  long double VDecayAnaltclThree = u_analy[beta];
#elif (defined pois_press || defined pois_PrFr)
  long double VDecayAnaltcl = u_analy[beta];
  long double VDecayAnaltclTwo = u_analy2[beta];
  long double VDecayAnaltclThree = u_analy3[beta];
  long double VDecayAnaltcl2 = VDecayAnaltcl;
#elif defined couette
  long double VDecayAnaltcl = u_analy[beta];
  long double VDecayAnaltcl2 = VDecayAnaltcl;
  long double VDecayAnaltclTwo = u_analy[beta];
  long double VDecayAnaltclThree = u_analy[beta];
#endif

#if (defined decay_kolmg)
  long double VDecayLBM = v[alfa][beta];
  long double V2DecayLBM = u[beta2][alfa2];
#elif (defined sound_waves || defined Doubly_periodic_shear_layers)
  long double VDecayLBM = rho[alfa][beta] - 1.0;
  long double V2DecayLBM = rho[beta2][alfa2];
#elif defined(forced_kolmg)
  long double VDecayLBM = v[alfa][beta];
  long double V2DecayLBM = u[alfa][beta];
#elif (defined pois_press || defined pois_PrFr)
  long double VDecayLBM = u[alfa][beta];
  long double V2DecayLBM = v[alfa][beta];
#elif defined couette
  long double VDecayLBM = u[alfa][beta];
  long double V2DecayLBM = v[alfa][beta];
#endif
  ///calculate error sum during iterations
  if (iteration <= WriteIter)
  {
    //err_iter+=fabsl(VDecayLBM-VDecayAnaltcl)/(u_max);
    err_iter += L2;
    L2_counter = L2_counter + 1;
  }

  if (b1 == 0)
  {
#if (defined sound_waves)
    char st1[] = "   \"DensityPurtrbed/densMAxPerturbed\", \"u\",\"Iteration\", \"density\", \"v\", \"1\", \"1\", \"L2\", \"L2Y\",\"Energy\" ";
#else
    char st1[] = "   \"VLBM\", \"VAnaltcl\",\"Iteration\", \"VLBM2\", \"VAnaltcl2\", \"VAnaltclTwo\", \"VAnaltclThree\", \"L2\", \"L2Y\" ";
#endif
    fprintf(hppp, "TITLE Iteration = \"%d\" \n", iteration);
    fprintf(hppp, "VARIABLES = %s\n", st1);
    fprintf(hppp, "ZONE I=%d%s \n", NY, ", F= POINT");
    ///////////////
    FILE *gp;
    char buf2[65];
    sprintf(buf2, "./output/plotEachstep%d.l", En);
    gp = fopen(buf2, "w");
#if (defined sound_waves)
    fprintf(gp, "set title \" {/Symbol r}' vs. time\" \n");
    fprintf(gp, "set ylabel \"{/Symbol r}'/{/Symbol r}'_{max}\" \n");
    fprintf(gp, "set xlabel \"time\" \n");
#else
    fprintf(gp, "set title \"Vvel vs. time\" \n");
    fprintf(gp, "set ylabel \"Vvel\" \n");
    fprintf(gp, "set xlabel \"time\" \n");
#endif

    //fprintf(gp, "set xrange [1:%d] \n",WriteIter);

#if (defined pois_press || defined pois_PrFr)
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:1 title 'LBM output %d{/Symbol p}/L' with lp,", En, En);
    fprintf(gp, " \"writeEachStep%d.dat\" using 3:2 title 'Analitical output %d{/Symbol p}/L' with lines,", En, En);
    fprintf(gp, " \"writeEachStep%d.dat\" using 3:6 title 'Analitical output2 %d{/Symbol p}/L' with lines", En, En);
//fprintf(gp, " \"writeEachStep%d.dat\" using 3:7 title 'Analitical output3 %d{/Symbol p}/L' with lines",En,En);
//anlaytical output two has compressibility effect at knudsen zero
#elif (defined forced_kolmg)
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:1 title 'LBM output %d{/Symbol p}/L' with lp,", En, En);
    fprintf(gp, " \"writeEachStep%d.dat\" using 3:2 title 'Estimation of Analitical output %d{/Symbol p}/L' with lines", En, En);
#elif (defined sound_waves)
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:1 title 'LBM output %d{/Symbol p}/L' with lp", En, En);
#else
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:1 title 'LBM output %d{/Symbol p}/L' with lp,", En, En);
    fprintf(gp, " \"writeEachStep%d.dat\" using 3:2 title 'Analitical output %d{/Symbol p}/L' with lines", En, En);
#endif

#if (defined sound_waves)

    fprintf(gp, "\n pause -1000 \"Hit any key to continue\" ");
    ///
    fprintf(gp, "\nset title \"Energy vs. Iteration\" \n");
    fprintf(gp, "set ylabel \"Energy\" \n");
    fprintf(gp, "set xlabel \"Iteration\" \n");

    //fprintf(gp, "set xrange [1:%d] \n",WriteIter);
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:10 title 'LBM output %d{/Symbol p}/L' with lp", En, En);
    ///

    fprintf(gp, "\n pause -1000 \"Hit any key to continue\" ");

    fprintf(gp, "\nset title \"u vs. Iteration\" \n");
    fprintf(gp, "set ylabel \"u\" \n");
    fprintf(gp, "set xlabel \"Iteration\" \n");

    //fprintf(gp, "set xrange [1:%d] \n",WriteIter);
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:2 title 'LBM output %d{/Symbol p}/L' with lp", En, En);
    ///

    fprintf(gp, "\n pause -1000 \"Hit any key to continue\" ");
    ///
    fprintf(gp, "\nset title \"v vs. Iteration\" \n");
    fprintf(gp, "set ylabel \"v\" \n");
    fprintf(gp, "set xlabel \"Iteration\" \n");

    //fprintf(gp, "set xrange [1:%d] \n",WriteIter);
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:5 title 'LBM output %d{/Symbol p}/L' with lp", En, En);
    ///

    fprintf(gp, "\n pause -1000 \"Hit any key to continue\" ");
    ///
    fprintf(gp, "\nset title \"Density vs. Iteration\" \n");
    fprintf(gp, "set ylabel \"Density\" \n");
    fprintf(gp, "set xlabel \"Iteration\" \n");

    //fprintf(gp, "set xrange [1:%d] \n",WriteIter);
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:4 title 'LBM output %d{/Symbol p}/L' with lp", En, En);

#else

    fprintf(gp, "\n pause -1000 \"Hit any key to continue\" ");
    ///
    fprintf(gp, "\nset title \"V2vel vs. Iteration\" \n");
    fprintf(gp, "set ylabel \"V2vel\" \n");
    fprintf(gp, "set xlabel \"Iteration\" \n");

    fprintf(gp, "set xrange [1:%d] \n", WriteIter);
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:4 title 'LBM output %d{/Symbol p}/L' with lp,", En, En);
#endif

#if (defined forced_kolmg)
    fprintf(gp, " \"writeEachStep%d.dat\" using 3:5 title 'Estimation of Analitical output %d{/Symbol p}/L' with lines", En, En);
#elif (defined sound_waves)
    fprintf(gp, " ");
#else
    fprintf(gp, " \"writeEachStep%d.dat\" using 3:5 title 'Analitical output %d{/Symbol p}/L' with lines", En, En);
#endif
    fprintf(gp, "\npause -1000 \"Hit any key to continue\" ");
    //
    fprintf(gp, "\nset title \"L2 error vs. Iteration\" \n");
    fprintf(gp, "set ylabel \"L2_Err\" \n");
    fprintf(gp, "set xlabel \"Iteration\" \n");

    fprintf(gp, "set xrange [1:%d] \n", WriteIter);
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:8 title 'LBM output %d{/Symbol p}/L' with lp", En, En);
    fprintf(gp, "\npause -1000 \"Hit any key to continue\" ");
    //
    //
    fprintf(gp, "\nset title \"L2 second direction error vs. Iteration\" \n");
    fprintf(gp, "set ylabel \"L2_Err\" \n");
    fprintf(gp, "set xlabel \"Iteration\" \n");

    fprintf(gp, "set xrange [1:%d] \n", WriteIter);
    fprintf(gp, "plot \"writeEachStep%d.dat\" using 3:9 title 'LBM output %d{/Symbol p}/L' with lp", En, En);
    fprintf(gp, "\npause -1000 \"Hit any key to continue\" ");
    //
    fclose(gp);
    ////////////
  }
  //fprintf(hppp, "#at Xcell = \"%d\" Xlocation = \"%Le\" \n", alfa,xalfa);

#if (defined sound_waves)
  fprintf(hppp, "%Le %Le %d %Le %Le %Le %Le %Le %Le %Le\n", VDecayLBM / (dens_max), VDecayAnaltcl, iteration,
          V2DecayLBM, VDecayAnaltcl2, VDecayAnaltclTwo, VDecayAnaltclThree, L2, L2Y, a11);

#else
  if (u_max2 == 0.)
  {
    fprintf(hppp, "%Le %Le %d %Le %Le %Le %Le %Le %Le\n", VDecayLBM / (u_max), VDecayAnaltcl / (u_max), iteration,
            V2DecayLBM, VDecayAnaltcl2, VDecayAnaltclTwo / (u_max), VDecayAnaltclThree / (u_max), L2, L2Y);
  }
  else
  {
    fprintf(hppp, "%Le %Le %d %Le %Le %Le %Le %Le %Le\n", VDecayLBM / (u_max), VDecayAnaltcl / (u_max), iteration,
            V2DecayLBM / (u_max2), VDecayAnaltcl2 / (u_max2), VDecayAnaltclTwo / (u_max), VDecayAnaltclThree / (u_max), L2, L2Y);
  }
#endif

  if ((iteration >= WriteIter) && (L2errorwriter == 0))
  {
    L2errorwriter = 1;
    fprintf(hppp, "##errorIS: %Le\n", err_iter);

    FILE *ffpm;
    ffpm = fopen("pre-program_Selction_Explained.txt", "a+");
    fprintf(ffpm, "\nL2error summation until WriteIter is: %Le \n", err_iter);
    fprintf(ffpm, "its average over iterations is: %Le \n", err_iter / L2_counter);
    fprintf(ffpm, "============================================================\n");
    fclose(ffpm);
  }

  fclose(hppp);
}

////////////////////////////////////////////////////////////////////
void writeEachStepDiag()
{
  FILE *hppp1;
  char buf11[75];
  long double Vdiag, loc1;
  int a22;
  int alfa = NX / (2 * En);
  long double VDecayLBM = sqrtl((u[alfa][alfa] * u[alfa][alfa]) + (v[alfa][alfa] * v[alfa][alfa]));
  sprintf(buf11, "./output/writeEachStepDiag%d.dat", En);
  if (iteration == 1)
  {
    hppp1 = fopen(buf11, "w");
  }
  else
  {
    hppp1 = fopen(buf11, "a+");
  }

  if (b1 == 0)
  {
    char st11[] = "   \"VLBM\",\"Iteration\" ";
    fprintf(hppp1, "TITLE Iteration= \"%d\" \n", iteration);
    fprintf(hppp1, "VARIABLES = %s\n", st11);
    fprintf(hppp1, "ZONE I=%d%s \n", 160, ", F= POINT");
    ///////////////
    FILE *gp2;
    char buf22[65];
    sprintf(buf22, "./output/plotEachstepDiag%d.l", En);
    gp2 = fopen(buf22, "w");
    fprintf(gp2, "set title \"Vvel vs. Iteration\" \n");
    fprintf(gp2, "set ylabel \"Vvel\" \n");
    fprintf(gp2, "set xlabel \"Iteration\" \n");
    fprintf(gp2, "set xrange [1:100] \n");
    fprintf(gp2, "plot \"writeEachStepDiag%d.dat\" using 2:1 title 'LBM output %d{/Symbol p}/L' with lp", En, En);
    fprintf(gp2, "\npause -1000 \"Hit any key to continue\" ");
    fclose(gp2);
    ////////////
  }
  //fprintf(hppp, "#at Xcell = \"%d\" Xlocation = \"%Le\" \n", alfa,xalfa);
  fprintf(hppp1, "%Le %d\n", VDecayLBM / (u_max), iteration);
  fclose(hppp1);

  if (b1 == 0)
  {
    FILE *hp;
    hp = fopen("./output/Out_GraphXYStart.dat", "w");
    char strr11[] = "   \"Y\", \"UV\" ";
    fprintf(hp, "TITLE Iteration= \"%d\" \n", iteration);
    fprintf(hp, "VARIABLES = %s\n", strr11);
    int a22 = a11;
    fprintf(hp, "ZONE I=%d%s \n", a22, ", F= POINT");

    for (int j = 0; j < NY; j++)
    {
      for (int i = 0; i < NX; i++)
      {
        if (i == j)
        {
          loc1 = sqrtl(x[i] * x[i] + y[j] * y[j]);
          if (v[i][j] >= 0)
            Vdiag = sqrtl((u[i][j] * u[i][j]) + (v[i][j] * v[i][j]));
          else
            Vdiag = -sqrtl((u[i][j] * u[i][j]) + (v[i][j] * v[i][j]));
          fprintf(hp, "%Le %Le\n", loc1, Vdiag);
        }
      }
    }
    fclose(hp); //Graph
  }
}
///

////////////////////////////////////////////////////////////////////
void L2error()
{
  long double sum_error = 0.;
  long double sum_errorY = 0.;
  long double mean11 = 0.;
  long double mean22 = 0.;
  long double mean11Y = 0.;
  long double mean22Y = 0.;
  sum_error = 0.;
  sum_errorY = 0.;
  L2 = 0.;
  L2Y = 0.;
  mean11 = 0.;
  mean22 = 0.;
  mean11Y = 0.;
  mean22Y = 0.;

#ifdef couette
  U_analyCalculation(2);
  for (int i = 0; i < NX; i++)
  {
    mean11 = 0.;
    mean22 = 0.;
    for (int j = 0; j < NY; j++)
    {
      mean11 += (u[i][j] - u_analy[j]) * (u[i][j] - u_analy[j]);
      mean22 += (u_analy[j]) * (u_analy[j]);
    }
    mean11 = sqrtl(mean11);
    mean22 = sqrtl(mean22);
    error[i] = mean11 / mean22;
    sum_error += error[i];
  }
  L2 = 1. / NX * sum_error;
  L2Y = L2;
#endif

#if (defined pois_press || defined pois_PrFr)
  U_analyCalculation(2);
  mean11 = 0.;
  mean22 = 0.;
  for (int i = 1; i < NX - 1; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      mean11 += (u[i][j] - u_analy[j]) * (u[i][j] - u_analy[j]);
      mean22 += (u_analy[j]) * (u_analy[j]);
    }
  }
  mean11 = sqrtl(mean11);
  mean22 = sqrtl(mean22);
  //error[i] = mean11/mean22  ;
  //sum_error+=error[i];
  //L2 = 1./NX*sum_error;
  L2 = mean11 / mean22;
  L2Y = L2;
#endif

//
#ifdef forced_kolmg
  U_analyCalculation(2);
  mean11 = 0.;
  mean22 = 0.;
  mean11Y = 0.;
  mean22Y = 0.;
  sum_error = 0.0;
  sum_errorY = 0.0;
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      mean11 += (v[i][j] - u_analx[i]) * (v[i][j] - u_analx[i]);
      mean22 += (u_analx[i]) * (u_analx[i]);
      mean11Y += (u[i][j] - u_analy[j]) * (u[i][j] - u_analy[j]);
      mean22Y += (u_analy[j]) * (u_analy[j]);
    }
  }
  mean11 = sqrtl(mean11);
  mean22 = sqrtl(mean22);
  mean11Y = sqrtl(mean11Y);
  mean22Y = sqrtl(mean22Y);
  if (mean22 == 0.0)
    mean22 = 1.;
  if (mean22Y == 0.0)
    mean22Y = 1.;
  ////sum_error=mean11/mean22;
  ////sum_errorY=mean11Y/mean22Y;
  L2 = mean11 / mean22;
  L2Y = mean11Y / mean22Y;
//L2 = 1./NY*sum_error;
//L2Y =1./NX*sum_errorY;
#endif

#ifdef decay_kolmg
  U_analyCalculation(1);
  mean11 = 0.;
  mean22 = 0.;
  mean11Y = 0.;
  mean22Y = 0.;
  sum_error = 0.0;
  sum_errorY = 0.0;
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      mean11 += (v[i][j] - u_analx[i]) * (v[i][j] - u_analx[i]);
      mean22 += (v[i][j]) * (v[i][j]);
      mean11Y += (u[i][j] - u_analy[j]) * (u[i][j] - u_analy[j]);
      mean22Y += (u[i][j]) * (u[i][j]);
    }
  }

  mean11 = sqrtl(mean11);
  mean22 = sqrtl(mean22);
  mean11Y = sqrtl(mean11Y);
  mean22Y = sqrtl(mean22Y);
  if (mean22 == 0.0)
    mean22 = 1.;
  if (mean22Y == 0.0)
    mean22Y = 1.;
  ////sum_error=mean11/mean22;
  ////sum_errorY=mean11Y/mean22Y;
  L2 = mean11 / mean22;
  L2Y = mean11Y / mean22Y;
//L2 = 1./NY*sum_error;
//L2Y =1./NX*sum_errorY;
#endif

  ///

#ifdef sound_waves
  U_analyCalculation(1);
  mean11 = 0.;
  mean22 = 0.;
  mean11Y = 0.;
  mean22Y = 0.;
  sum_error = 0.0;
  sum_errorY = 0.0;
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      mean11 += (rho[i][j] - u_analx[i]) * (rho[i][j] - u_analx[i]);
      mean22 += (rho[i][j]) * (rho[i][j]);
      mean11Y += (rho[i][j] - u_analy[j]) * (rho[i][j] - u_analy[j]);
      mean22Y += (rho[i][j]) * (rho[i][j]);
    }
  }

  mean11 = sqrtl(mean11);
  mean22 = sqrtl(mean22);
  mean11Y = sqrtl(mean11Y);
  mean22Y = sqrtl(mean22Y);
  if (mean22 == 0.0)
    mean22 = 1.;
  if (mean22Y == 0.0)
    mean22Y = 1.;
  ////sum_error=mean11/mean22;
  ////sum_errorY=mean11Y/mean22Y;
  L2 = mean11 / mean22;
  L2Y = mean11Y / mean22Y;
//L2 = 1./NY*sum_error;
//L2Y =1./NX*sum_errorY;
#endif

  ///
}

////////////////////////////////////////////////////////////////////
void collision()
{

  // collision step
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      for (int k = 0; k < NPOP; k++)
      {
        //fBar[i][j][k]=fprop[i][j][k]+0.5*1/(tau-0.0)*(fprop[i][j][k]-feqP[i][j][k]); //from Dellar paper (euation 39)
        fBar[i][j][k] = fBarprop[i][j][k] - 1. / (tau + 0.0) * (fBarprop[i][j][k] - feq[i][j][k]); //from Dellar paper (euation 40)

#if (defined forced_kolmg || defined pois_PrFr)
        f[i][j][k] = (1. - omega) * fprop[i][j][k] + omega * feq[i][j][k] + Force[i][j][k];
#elif (defined pois_press || defined couette || defined decay_kolmg || defined sound_waves || defined Doubly_periodic_shear_layers)
        f[i][j][k] = (1. - omega) * fprop[i][j][k] + omega * feq[i][j][k];
#endif
      }
    }
  } // collision step
}

////////////////////////////////////////////////////////////////////
void EquilibriumCalculation()
{
  // compute equilibrium distribution
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      UTwo = u[i][j] * u[i][j] + v[i][j] * v[i][j];
      for (int k = 0; k < NPOP; k++)
      {
        //fBar[i][j][k]=f[i][j][k]+0.5*1/(tau+0.0)*(f[i][j][k]-feq[i][j][k]); //from Dellar paper (euation 39)
        //fBar[i][j][k]=fBar[i][j][k]-1/(tau+0.5)*(fBar[i][j][k]-feq[i][j][k]); //from Dellar paper (euation 40)
        ZetaU = u[i][j] * cx[k] + v[i][j] * cy[k];

#ifdef EquilFuncL
        feq[i][j][k] = w[k] * (rho[i][j] + (1. / RT) * ZetaU); //Linearized equilibrium distribution (4.38)
#endif

#ifdef EquilFuncS
        ///feq[i][j][k] = w[k]*rho[i][j]*(   1. + (1./(c_sound*c_sound))*(u[i][j]*cx[k]+v[i][j]*cy[k])+
        ///(1./(2.*c_sound*c_sound*c_sound*c_sound))*(u[i][j]*cx[k]+v[i][j]*cy[k])*(u[i][j]*cx[k]+v[i][j]*cy[k])-
        ///(1./(2.*c_sound*c_sound))* (u[i][j]*u[i][j]+v[i][j]*v[i][j])   );
        FourthTerm = (1. / 6.) * ((ZetaU * ZetaU) / (RT * RT) - 3 * UTwo / RT) * (ZetaU / RT);

        feq[i][j][k] = w[k] * rho[i][j] * (1. + ZetaU / RT + 1. / 2. * ZetaU * ZetaU / (RT * RT) - 1. / (2. * RT) * UTwo + HigherCoe * FourthTerm);

        feqP[i][j][k] = feq[i][j][k];
#endif
        //fBar[i][j][k]=f[i][j][k]+0.5*1/(tau-0.5)*(f[i][j][k]-feq[i][j][k]); //from Dellar paper (euation 39)
      }
    }
  } // compute equilibrium distribution

//compute forcing term in LBE
#if (defined forced_kolmg || defined pois_PrFr)

  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      Zigma11 = 0.0;
      Zigma12 = 0.0;
      Zigma21 = 0.0;
      Zigma22 = 0.0;
      UTwo = u[i][j] * u[i][j] + v[i][j] * v[i][j];
      UF = fx[i][j] * u[i][j] + fy[i][j] * v[i][j];

      for (int k = 0; k < NPOP; k++)
      {
        Zigma11 += (f[i][j][k] - feq[i][j][k]) * cx[k] * cx[k];
        Zigma12 += (f[i][j][k] - feq[i][j][k]) * cx[k] * cy[k];
        Zigma21 += (f[i][j][k] - feq[i][j][k]) * cy[k] * cx[k];
        Zigma22 += (f[i][j][k] - feq[i][j][k]) * cy[k] * cy[k];
      }
      Zigma11 += rho[i][j] * UTwo;
      Zigma12 += rho[i][j] * UTwo;
      Zigma21 += rho[i][j] * UTwo;
      Zigma22 += rho[i][j] * UTwo;
      Zigma11 = Zigma11 / (2 * rho[i][j] * RT);
      Zigma12 = Zigma12 / (2 * rho[i][j] * RT);
      Zigma21 = Zigma21 / (2 * rho[i][j] * RT);
      Zigma22 = Zigma22 / (2 * rho[i][j] * RT);

      for (int k = 0; k < NPOP; k++)
      {
        ZetaU = u[i][j] * cx[k] + v[i][j] * cy[k];
        ZetaF = fx[i][j] * cx[k] + fy[i][j] * cy[k];
        HermTWO11 = (cx[k] / RT) * (cx[k] / RT) - 1.0;
        HermTWO12 = (cx[k] / RT) * (cy[k] / RT);
        HermTWO21 = (cy[k] / RT) * (cx[k] / RT);
        HermTWO22 = (cy[k] / RT) * (cy[k] / RT) - 1.0;

        HermTWO11 = (HermTWO11 * ZetaF / RT) - (2 * ZetaF / RT);
        HermTWO12 = (HermTWO12 * ZetaF / RT) - (2 * ZetaF / RT);
        HermTWO21 = (HermTWO21 * ZetaF / RT) - (2 * ZetaF / RT);
        HermTWO22 = (HermTWO22 * ZetaF / RT) - (2 * ZetaF / RT);

        FourthTerm = Zigma11 * HermTWO11 + Zigma12 * HermTWO12 +
                     Zigma21 * HermTWO21 + Zigma22 * HermTWO22;

#ifdef EquilFuncL
        Force[i][j][k] = (1. - omega / 2.) * w[k] * ZetaF / RT;
#endif

#ifdef EquilFuncS
        Force[i][j][k] = (1. - omega / 2.) * w[k] * rho[i][j] *
                             (ZetaF / RT + ZetaF * ZetaU / (RT * RT) - UF / RT) +
                         HigherCoe * FourthTerm;

        // Force[i][j][k]=(1.-omega/2.)*w[k]*
        //(
        //   ( (1./RT) *(cx[k]-u[i][j])
        //// +(1./(RT*RT))*(cx[k]*u[i][j]+cy[k]*v[i][j])
        //*cx[k])*fx[i][j]+( (1./RT) *(cy[k]-v[i][j])+ (1./(RT*RT)) *(cx[k]*u[i][j]+cy[k]*v[i][j])*cy[k])*fy[i][j]
        //);

#endif
      }
    }
  }
#endif //compute forcing term in LBE

///////
#ifdef DELLAR_ONE
  a11 = nuPrimeRatio / 2. - 1. / 3.;

  TrPiConst = 1.0 + 1.0 / (2. * (tau - 0.5)) + D * (nuPrimeRatio / 2.0 - 1.0 / 3.0);
  //calculate TrPiOne
  for (int j = 0; j < NY; j++)
  {
    for (int i = 0; i < NX; i++)
    {
      TrPiBar[i][j] = 0.0;
      TrPiOne[i][j] = 0.0;
      UTwo = u[i][j] * u[i][j] + v[i][j] * v[i][j];
      for (int k = 0; k < NPOP; k++)
      {
        TrPiBar[i][j] += cx[k] * cx[k] * fBarprop[i][j][k] + cy[k] * cy[k] * fBarprop[i][j][k];
      }
      TrPiOne[i][j] = (TrPiBar[i][j] - D * rho[i][j] * RT - rho[i][j] * UTwo) / TrPiConst;
      DivU[i][j] = -TrPiOne[i][j] / (2 * rho[i][j] * RT * (tau - 0.5));
    }
  }
  ///calculate new equilibrium based on TrPiOne
  for (int j = 0; j < NY; j++)
  {
    for (int i = 0; i < NX; i++)
    {
      for (int k = 0; k < NPOP; k++)
      {
        ZetaTwo = cx[k] * cx[k] + cy[k] * cy[k];
        b11 = (ZetaTwo - D * RT) / (2.0 * RT * RT);

        feq[i][j][k] = feq[i][j][k] + w[k] * (a11) * (b11)*TrPiOne[i][j];
      }
    }
  }
#endif
  ///
}

////////////////////////////////////////////////////////////////////
void initialization()
{

  finisher = 0;
  iteration = 0;
  finisher2 = 0;
  L2errorwriter = 0;
  b1 = 0;
  conv_counter = 0;
  L2_counter = 0;
  err_iter = 0.;
  //dynamic allocation of arrays
  w = malloc(NPOP * sizeof(long double));
  cx = malloc(NPOP * sizeof(long double));
  cy = malloc(NPOP * sizeof(long double));
  x = malloc(NX * sizeof(long double));
  y = malloc(NY * sizeof(long double));
  u_analy = malloc(NY * sizeof(long double));
  u_analx = malloc(NX * sizeof(long double));
  u_analy2 = malloc(NY * sizeof(long double));
  u_analy3 = malloc(NY * sizeof(long double));
  error = malloc(NX * sizeof(long double)); //dynamic allocation of arrays
  feq = AllocateArray3D(NX, NY, NPOP);
  feqP = AllocateArray3D(NX, NY, NPOP);
  f = AllocateArray3D(NX, NY, NPOP);
  fBar = AllocateArray3D(NX, NY, NPOP);
  fBarprop = AllocateArray3D(NX, NY, NPOP);
  fprop = AllocateArray3D(NX, NY, NPOP);
  Force = AllocateArray3D(NX, NY, NPOP);
  u_old = AllocateArray2D(NX, NY);
  rho = AllocateArray2D(NX, NY);
  u = AllocateArray2D(NX, NY);
  v = AllocateArray2D(NX, NY);
  vorticity = AllocateArray2D(NX, NY);
  fx = AllocateArray2D(NX, NY);
  fy = AllocateArray2D(NX, NY);
  TrPiBar = AllocateArray2D(NX, NY);
  TrPiOne = AllocateArray2D(NX, NY);
  DivU = AllocateArray2D(NX, NY);
/////////////////////////////////////////////////////////////////////
#ifdef D2Q9                                                                                         //  0      1     2     3     4      5      6       7      8
  long double ww[] = {1 / 9., 1 / 9., 1 / 9., 1 / 9., 1 / 36., 1 / 36., 1 / 36., 1 / 36., 4. / 9.}; // weights
  long double cxx[] = {1., 0., -1., 0., 1., -1., -1., 1., 0.};                                      // lattice velocities, x components
  long double cyy[] = {0., 1., 0., -1., 1., 1., -1., -1., 0.};                                      // lattice velocities, y components
                                                                                                    /*
       5        1       4

       2        8       0
       
       6        3       7           
 
 
 */
#endif
#ifdef D2Q21                                                                                                                                                                                                                                               //  0       1       2       3       4       5       6       7       8         9        10       11       12        13         14        15        16       17        18        19        20
  long double ww[] = {1. / 12., 1. / 12., 1. / 12., 1. / 12., 2. / 27., 2. / 27., 2. / 27., 2. / 27., 91. / 324., 7. / 360., 7. / 360., 7. / 360., 7. / 360., 1. / 432., 1. / 432., 1. / 432., 1. / 432., 1. / 1620., 1. / 1620., 1. / 1620., 1. / 1620.}; // weights
  long double cxx[] = {1., 0., -1., 0., 1., -1., -1., 1., 0.0, 2.0, 0.0, -2.0, 0.0, 2.0, -2.0, -2.0, 2.0, 3.0, 0.0, -3.0, 0.0};                                                                                                                            // lattice velocities, x components
  long double cyy[] = {0., 1., 0., -1., 1., 1., -1., -1., 0.0, 0.0, 2.0, 0.0, -2.0, 2.0, 2.0, -2.0, -2.0, 0.0, 3.0, 0.0, -3.0};                                                                                                                            // lattice velocities, y components
  /* 
                             18

            14               10              13

                     5       1       4

   19       11       2       8       0       9       17
       
                     6       3       7           
 
            15               12              16

                             20
 */
#endif
  Cs2Calculate = 0.;
  for (int k = 0; k < NPOP; k++)
  {
    w[k] = ww[k];
    cx[k] = cxx[k];
    cy[k] = cyy[k];
    Cs2Calculate += w[k] * (cx[k] * cx[k]);
  }
  //make zero
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      u_analy[j] = 0;
      u_analx[i] = 0;
      fx[i][j] = 0.0;
      fy[i][j] = 0.0;
    }
  } //make zero

  //geometry setting
  for (int i = 0; i < NX; i++)
  {
    x[i] = i + 0.5;
  }
  for (int i = 0; i < NY; i++)
  {
    y[i] = i + 0.5;
  } //geometry setting

  U_analyCalculation(2);

//force setting
#ifdef pois_PrFr
  for (int j = 0; j < NY; j++)
  {
    for (int i = 0; i < NX; i++)
    {
      //fx[i][j]=Fdensity*x[i]/(NX);
      fx[i][j] = Fdensity;
      //fx[i][j]=Fdensity*(i+1)/(NX+1);
      fy[i][j] = 0.0;
    }
  }
#endif
//
#ifdef decay_kolmg
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      fx[i][j] = 0.0;
      fy[i][j] = 0.0;
    }
  }
#endif
//
#ifdef forced_kolmg
  for (int j = 0; j < NY; j++)
  {
    for (int i = 0; i < NX; i++)
    {

      fx[i][j] = 0.0;

      fy[i][j] = Fkol * sinl(En * M_PI * (x[i]) / (NX));
    }
  }

#endif //force setting

  //Initialization
  for (int j = 0; j < NY; j++)
  {
    for (int i = 0; i < NX; i++)
    {

      rho[i][j] = 1.;
      u[i][j] = 0.;
      v[i][j] = 0.;
      vorticity[i][j] = 0.;
#if (defined decay_kolmg && !defined diagDecay)
      u[i][j] = u_max2 * sinl(En2 * M_PI * (y[j]) / (NY));
      v[i][j] = u_max * sinl(En * M_PI * (x[i]) / (NX));
      rho[i][j] = 1. + dens_max * sinl(En * M_PI * (x[i]) / (NX));
#endif
/////////////////// diagonal testing
#if (defined decay_kolmg && defined diagDecay)
      if (i == j)
      {
        a11 = (sqrtl(NX * NX + NY * NY));
        b11 = sqrtl(x[i] * x[i] + y[j] * y[j]);
        u[i][j] = -sinl(M_PI / 4.) * u_max * sinl(En * M_PI * (b11) / (a11));
        v[i][j] = sinl(M_PI / 4.) * u_max * sinl(En * M_PI * (b11) / (a11));
      }
#endif
      //////////soundwave//////////
#if (defined sound_waves)
      int a122 = NY / 2;
      rho[i][j] = 1. + dens_max * sinl(En * M_PI * (x[i]) / (NX));
      u[i][j] = 0.0;
      //u[i][j]=dens_max*sinl(En*M_PI*(x[i])/(NX));
      //rho[i][a122]=1.+dens_max*sinl(En*M_PI*(x[i])/(NX));
#endif
      ///////////////////////
      //////////soundwave//////////
#if (defined Doubly_periodic_shear_layers)
      int a122 = NY / 2;

      if (y[j] <= 0.5 * NY)
      {
        u[i][j] = u_max * tanhl(80 * (y[j] / NY - 1 / 4.));
      }
      else if (y[j] > 0.5 * NY)
      {
        u[i][j] = u_max * tanhl(80 * (3 / 4. - y[j] / NY));
      }
      else
      {
        printf("\n found an undetermined position 1");
        getchar();
      }

      v[i][j] = u_max2 * sinl(2 * M_PI * (x[i] / NX + 1 / 4.));
      rho[i][j] = 1.;
#endif
      ///////////////////////
      u_old[i][j] = u_max;

#if (defined sound_waves || defined Doubly_periodic_shear_layers)
      u_old[i][j] = 1.0;
#endif

      UTwo = u[i][j] * u[i][j] + v[i][j] * v[i][j];
      ////
      for (int k = 0; k < NPOP; k++)
      {
        ////

        ZetaU = u[i][j] * cx[k] + v[i][j] * cy[k];
        ZetaF = fx[i][j] * cx[k] + fy[i][j] * cy[k];
        UF = fx[i][j] * u[i][j] + fy[i][j] * v[i][j];
        // #if (defined couette || defined pois_press)
        //  feq[i][j][k]=w[k];
        // #elif defined(pois_PrFr)
        //feq[i][j][k]=w[k]*(rho[i][j] + (1./(c_sound*c_sound))*(u[i][j]*cx[k]+v[i][j]*cy[k]));

        //Force[i][j][k]=(1.-omega/2.)*w[k]*(1./(c_sound*c_sound))*(cx[k]*fx[i][j]+cy[k]*fy[i][j]);
        //#elif  (defined decay_kolmg || defined forced_kolmg)

#ifdef EquilFuncL
        feq[i][j][k] = w[k] * (rho[i][j] + (1. / RT) * ZetaU); //Linearized equilibrium distribution (4.38)
        Force[i][j][k] = (1. - omega / 2.) * w[k] * ZetaF / RT;
#endif

#ifdef EquilFuncS
        FourthTerm = (1. / 6.) * ((ZetaU * ZetaU) / (RT * RT) - 3 * UTwo / RT) * (ZetaU / RT);

        feq[i][j][k] = w[k] * rho[i][j] * (1. + ZetaU / RT + 1. / 2. * ZetaU * ZetaU / (RT * RT) - 1. / (2. * RT) * UTwo + HigherCoe * FourthTerm);

        Force[i][j][k] = (1. - omega / 2.) * w[k] * rho[i][j] *
                         (ZetaF / RT + ZetaF * ZetaU / (RT * RT) - UF / RT);
        //Force[i][j][k]=(1.-omega/2.)*w[k]*( ( (1./RT) *(cx[k]-u[i][j])+(1./(RT*RT))*(cx[k]*u[i][j]+cy[k]*v[i][j])
        //*cx[k])*fx[i][j]+( (1./RT) *(cy[k]-v[i][j])+ (1./(RT*RT)) *(cx[k]*u[i][j]+cy[k]*v[i][j])
        //*cy[k])*fy[i][j]);

#endif
        feqP[i][j][k] = feq[i][j][k];
        // #endif

        f[i][j][k] = feq[i][j][k];
        fprop[i][j][k] = feq[i][j][k];
        fBar[i][j][k] = f[i][j][k];
        fBarprop[i][j][k] = fBar[i][j][k];
        //+0.5*1/(tau+0.0)*(f[i][j][k]-feq[i][j][k]); //from Dellar paper (euation 39)
        //fBar[i][j][k]=fBar[i][j][k]-1/(tau+0.5)*(fBar[i][j][k]-feq[i][j][k]); //from Dellar paper (euation 40)
      }
    }
  } //Initialization
  ////
  ///Calculation of Vorticity
  newx = 0;
  newy = 0;

  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {

      newx = i + 1;
      newy = i - 1;
      if (i == NX - 1)
      {
        newx = 0;
      }
      if (i == 0)
      {
        newy = NX - 1;
      }
      a11 = NX * (v[newx][j] - v[newy][j]) / (2);
      newx = j + 1;
      newy = j - 1;
      if (j == NY - 1)
      {
        newx = 0;
      }
      if (j == 0)
      {
        newy = NY - 1;
      }
      b11 = NY * (u[i][newx] - u[i][newy]) / (2);

      vorticity[i][j] = a11 - b11;
    }
  }
  ///Calculation of Vorticity
  ///
  newx = 0;
  newy = 0;
  //write for time zero
  //kol0.dat
  FILE *fp;
  char buf[65];
  sprintf(buf, "./output/kol/Kol0.dat");
  fp = fopen(buf, "w");

#ifdef sound_waves
  char strr[] = "   \"X\", \"Density-perturbed/dens_max\" , \"u/(Csound*dens_max)\" , \"v\", \"Energy\" ";
#else
  char strr[] = "   \"X\", \"Y\" , \"u\" , \"v\", \"vorticity-numerical\", \"DivU\", \"vorticity-analytical\",\"(vort_Num-vort_Anal)/vort_Anal\"";
#endif

  fprintf(fp, "VARIABLES = %s\n", strr);
  fprintf(fp, "ZONE T= \"time %d\", I=%d, J=%d%s \n", iteration, NX, NY, ", F= POINT");
  fprintf(fp, "  \n");

  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      //Analytical vorticity at t=0
      if (y[j] <= 0.5 * NY)
      {
        a11 = 2 * M_PI * u_max2 * cosl(2 * M_PI * (x[i] / NX + 1 / 4.)) - u_max * 80 * (1 - tanhl(80 * (y[j] / NY - 1 / 4.)) * tanhl(80 * (y[j] / NY - 1 / 4.)));
      }
      else if (y[j] > 0.5 * NY)
      {
        a11 = 2 * M_PI * u_max2 * cosl(2 * M_PI * (x[i] / NX + 1 / 4.)) + u_max * 80 * (1 - tanhl(80 * (3 / 4. - y[j] / NY)) * tanhl(80 * (3 / 4. - y[j] / NY)));
      }
      else
      {
        printf("\n found an undetermined position 2");
        getchar();
      }
////
#ifdef sound_waves
      a11 = u[i][j] * u[i][j] + RT * (rho[i][j] - 1.0000) * (rho[i][j] - 1.0000);
      fprintf(fp, "%Le %Le %Le %Le %Le\n", x[i], (rho[i][j] - 1.0000) / dens_max, u[i][j] / (c_sound * dens_max), v[i][j], a11);
#else
      fprintf(fp, "%Le %Le %Le %Le %Le %Le %Le %Le\n", x[i] / NX, y[j] / NY, u[i][j], v[i][j], vorticity[i][j], a11 * 0.0, a11, (vorticity[i][j] - a11) / a11);
#endif

      //write for time zero
    }
    fprintf(fp, "  \n");
  }
  ///
  fclose(fp);

#ifdef DELLAR_ONE
  a11 = nuPrimeRatio / 2. - 1. / 3.;
  TrPiConst = 1.0 + 1.0 / (2. * (tau - 0.5)) + D * (nuPrimeRatio / 2.0 - 1.0 / 3.0);
  //calculate TrPiOne
  for (int j = 0; j < NY; j++)
  {
    for (int i = 0; i < NX; i++)
    {
      TrPiBar[i][j] = 0.0;
      TrPiOne[i][j] = 0.0;
      UTwo = u[i][j] * u[i][j] + v[i][j] * v[i][j];
      for (int k = 0; k < NPOP; k++)
      {
        TrPiBar[i][j] += cx[k] * cx[k] * fBar[i][j][k] + cy[k] * cy[k] * fBar[i][j][k];
      }
      TrPiOne[i][j] = (TrPiBar[i][j] - D * rho[i][j] * RT - rho[i][j] * UTwo) / TrPiConst;
      DivU[i][j] = -TrPiOne[i][j] / (2 * rho[i][j] * RT * (tau - 0.5));
    }
  }
  ///calculate new equilibrium based on TrPiOne
  for (int j = 0; j < NY; j++)
  {
    for (int i = 0; i < NX; i++)
    {
      for (int k = 0; k < NPOP; k++)
      {
        ZetaTwo = cx[k] * cx[k] + cy[k] * cy[k];
        b11 = (ZetaTwo - D * RT) / (2.0 * RT * RT);
        //feq[i][j][k]=feq[i][j][k]+w[k]*(a11)*(b11)*TrPiOne[i][j];
        //f[i][j][k]=feq[i][j][k];
        //fprop[i][j][k]=feq[i][j][k];
        //fBar[i][j][k]=f[i][j][k]+0.5/(tau+0.0)*(f[i][j][k]-feqP[i][j][k]);
        //fBarprop[i][j][k]=fBar[i][j][k];
      }
    }
  }
#endif
  ///

  FILE *ffpm;
  ffpm = fopen("pre-program_Selction_Explained.txt", "w");
  fprintf(ffpm, "\nNX: %d,NY: %d,CHARLENGTH: %Lf \n", NX, NY, CharLength);
  fprintf(ffpm, "\nNuPrimeRatio: %Lf\n", nuPrimeRatio);
#if (defined forced_kolmg || defined decay_kolmg)
  fprintf(ffpm, "eN1: %d,eN2: %d\n", En, En2);
#endif
  fprintf(ffpm, "Tau: %Lf,U1: %Lf,U2: %Lf \n", tau, u_max, u_max2);
  fprintf(ffpm, "Reynolds number is: %Lf, @nuo: %Lf \n", Re, nuo);
  fprintf(ffpm, "Knudsen number is: %Lf \n", Kn);

  fclose(ffpm);
}

////////////////////////////////////////////////////////////////////
void Macroscopic_Evaluation()
{
  //Macroscopic evaluation
  //density and velocities
  iteration = t;
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {
      rho[i][j] = 0.0;
      u[i][j] = 0.0;
      v[i][j] = 0.0;
      for (int k = 0; k < NPOP; k++)
      {
        //rho[i][j] = fprop[i][j][0]+fprop[i][j][1]+fprop[i][j][2]+fprop[i][j][3]+fprop[i][j][4]+fprop[i][j][5]+fprop[i][j][6]+fprop[i][j][7]+fprop[i][j][8];
        //u[i][j] = (fprop[i][j][0]+fprop[i][j][4]+fprop[i][j][7])-(fprop[i][j][2]+fprop[i][j][5]+fprop[i][j][6]);
        //v[i][j] = (fprop[i][j][1]+fprop[i][j][4]+fprop[i][j][5])-(fprop[i][j][3]+fprop[i][j][6]+fprop[i][j][7]);
        rho[i][j] += fprop[i][j][k];
        u[i][j] += fprop[i][j][k] * cx[k];
        v[i][j] += fprop[i][j][k] * cy[k];
      }
#if (defined pois_PrFr || defined forced_kolmg)
      //  u[i][j] += rho[i][j]*fx[i][j]/2.;
      //  v[i][j] += rho[i][j]*fy[i][j]/2.;
      u[i][j] += fx[i][j] / 2.;
      v[i][j] += fy[i][j] / 2.;
#endif
    }
  }

#ifdef EquilFuncS
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {

      u[i][j] = u[i][j] / rho[i][j];
      v[i][j] = v[i][j] / rho[i][j];
    }
  }
#endif

#ifdef EquilFuncL
  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {

      u[i][j] = u[i][j] / rho[i][j];
      v[i][j] = v[i][j] / rho[i][j];
    }
  }
#endif

  ///Calculation of Vorticity
  newx = 0;
  newy = 0;

  for (int i = 0; i < NX; i++)
  {
    for (int j = 0; j < NY; j++)
    {

      newx = i + 1;
      newy = i - 1;
      if (i == NX - 1)
      {
        newx = 0;
      }
      if (i == 0)
      {
        newy = NX - 1;
      }
      a11 = NX * (v[newx][j] - v[newy][j]) / (2);
      newx = j + 1;
      newy = j - 1;
      if (j == NY - 1)
      {
        newx = 0;
      }
      if (j == 0)
      {
        newy = NY - 1;
      }
      b11 = NY * (u[i][newx] - u[i][newy]) / (2);

      vorticity[i][j] = a11 - b11;
    }
  }
  ///Calculation of Vorticity
}

////////////////////////////////////////////////////////////////////
void CheckConvergence()
{

  if ((t % teval) == 0)
  {
    mean1 = 0.;
    mean2 = 0.;

#ifdef couette
    for (int i = 0; i < NX; i++)
    {
      for (int j = 0; j < NY; j++)
      {
        mean1 += u[i][j];
        mean2 += u_old[i][j];
      }
    }
#endif

#if (defined decay_kolmg || defined forced_kolmg)
    for (int i = 0; i < NX; i++)
    {
      for (int j = 0; j < NY; j++)
      {
        mean1 += v[i][j];
        mean2 += u_old[i][j];
      }
    }
#endif
//
#if (defined sound_waves)
    for (int i = 0; i < NX; i++)
    {
      for (int j = 0; j < NY; j++)
      {
        mean1 += rho[i][j];
        mean2 += u_old[i][j];
      }
    }
#endif
    //
    //
#if (defined Doubly_periodic_shear_layers)
    for (int i = 0; i < NX; i++)
    {
      for (int j = 0; j < NY; j++)
      {
        mean1 += fabsl(u[i][j]);
        mean2 += fabsl(u_old[i][j]);
      }
    }
#endif
//
#if (defined pois_press || defined pois_PrFr)
    for (int i = 1; i < NX - 1; i++)
    {
      for (int j = 0; j < NY; j++)
      {
        mean1 += u[i][j];
        mean2 += u_old[i][j];
      }
    }
#endif
    if ((mean2 == 0.) && (t > WriteItermode))
    {
      printf("\n mean vel zero, possible to finish the simulation");
      finisher = 1;
      write_Kolmo();
      finish_The_Simulation(); //break the code
    }
    conv = fabsl((mean1 / mean2) - 1.);

    if (conv < tol)
    {
      conv_counter += 1;
      if (conv_counter > 5)
      {
        printf("\n**conv<tol**");
        if (t == NSTEPS)
        {
          finisher = 1;
          write_Kolmo();
          printf("\nReach to the End of Simulation @NSTEPS@/\n");
          finish_The_Simulation();
        }
      }
    }
    else
    {
      ////
      for (int i = 0; i < NX; i++)
      {
        for (int j = 0; j < NY; j++)
        {
          u_old[i][j] = u[i][j];

#if (defined decay_kolmg || defined forced_kolmg)
          u_old[i][j] = v[i][j];
#endif
#if (defined sound_waves)
          u_old[i][j] = rho[i][j];
#endif
#if (defined Doubly_periodic_shear_layers)
          u_old[i][j] = u[i][j];
#endif
        }
      }
      ////
    }
  } //check convergence
}

////////////////////////////////////////////////////////////////////
void BoundaryCheck()
{
//Inlet&Outlet BC for Poiseuille pressure driven
//#if (defined pois_press || defined pois_PrFr)
#if (defined pois_press)
  for (int j = 0; j < NY; j++)
  {
    for (int k = 0; k < NPOP; k++)
    {

#ifdef EquilFuncL
      f[0][j][k] = w[k] * (rho_inlet + (1. / (c_sound * c_sound)) * (u[NX - 2][j] * cx[k] + v[NX - 2][j] * cy[k])) + f[NX - 2][j][k] - feq[NX - 2][j][k];

      f[NX - 1][j][k] = w[k] * (rho_outlet + (1. / (c_sound * c_sound)) * (u[1][j] * cx[k] + v[1][j] * cy[k])) + f[1][j][k] - feq[1][j][k];
#endif

#ifdef EquilFuncS

      f[0][j][k] = w[k] * rho_inlet * (1. + (1. / (c_sound * c_sound)) * (u[NX - 2][j] * cx[k] + v[NX - 2][j] * cy[k]) + (1. / (2. * c_sound * c_sound * c_sound * c_sound)) * (u[NX - 2][j] * cx[k] + v[NX - 2][j] * cy[k]) * (u[NX - 2][j] * cx[k] + v[NX - 2][j] * cy[k]) - (1. / (2. * c_sound * c_sound)) * (u[NX - 2][j] * u[NX - 2][j] + v[NX - 2][j] * v[NX - 2][j])) + f[NX - 2][j][k] - feq[NX - 2][j][k];
      //
      f[NX - 1][j][k] = w[k] * rho_outlet * (1. + (1. / (c_sound * c_sound)) * (u[1][j] * cx[k] + v[1][j] * cy[k]) + (1. / (2. * c_sound * c_sound * c_sound * c_sound)) * (u[1][j] * cx[k] + v[1][j] * cy[k]) * (u[1][j] * cx[k] + v[1][j] * cy[k]) - (1. / (2. * c_sound * c_sound)) * (u[1][j] * u[1][j] + v[1][j] * v[1][j])) + f[1][j][k] - feq[1][j][k];
#endif
    }
  }

#endif //Inlet&Outlet BC fir Poiseuille pressure driven
}

////////////////////////////////////////////////////////////////////
void Stream_and_WallReflection()
{

  //Streaming step
  for (int k = 1; k < NPOP + 1; k++)
  {
    for (int j = 1; j < NY + 1; j++)
    {
      for (int i = 1; i < NX + 1; i++)
      {
        newx = 1 + ((int)((i - 1 + cx[k - 1] + NX)) % (NX));
        newy = 1 + ((int)((j - 1 + cy[k - 1] + NY)) % (NY));
        fprop[newx - 1][newy - 1][k - 1] = f[i - 1][j - 1][k - 1];
        fBarprop[newx - 1][newy - 1][k - 1] = fBar[i - 1][j - 1][k - 1];
      }
    }
  } //Streaming step

  // Boundary condition (bounce-back)
  // Top wall (moving with tangential velocity u_max)
  for (int i = 0; i < NX; i++)
  {
#ifdef couette
#ifdef D2Q9
    //Upper Wall: Only layer 1 is engaged
    fprop[i][NY - 1][3] = f[i][NY - 1][1];
    fprop[i][NY - 1][6] = f[i][NY - 1][4] + 2 * w[6] * cx[6] / RT * u_max; //(1./6.)*u_max;
    fprop[i][NY - 1][7] = f[i][NY - 1][5] + 2 * w[7] * cx[7] / RT * u_max; //(1./6.)*u_max;
    //Bottom Wall: Only layer 1 is engaged
    fprop[i][0][1] = f[i][0][3];
    fprop[i][0][4] = f[i][0][6];
    fprop[i][0][5] = f[i][0][7];
#endif

#ifdef D2Q21
    //Upper Wall: 3 Layers are engaged: NY-1, NY-2, NY-3
    //Layer 1
    fprop[i][NY - 1][3] = f[i][NY - 1][1];
    fprop[i][NY - 1][6] = f[i][NY - 1][4] + 2 * w[6] * cx[6] / RT * u_max; //(1./6.)*u_max;
    fprop[i][NY - 1][7] = f[i][NY - 1][5] + 2 * w[7] * cx[7] / RT * u_max; //(1./6.)*u_max;

    fprop[i][NY - 2][12] = f[i][NY - 1][10];
    fprop[i][NY - 3][20] = f[i][NY - 1][18];
    fprop[i][NY - 2][15] = f[i][NY - 1][13] + 2 * cx[15] * w[15] / RT * u_max;
    fprop[i][NY - 2][16] = f[i][NY - 1][14] + 2 * cx[16] * w[16] / RT * u_max;
    //Layer 2
    fprop[i][NY - 1][12] = f[i][NY - 2][10];
    fprop[i][NY - 2][20] = f[i][NY - 2][18];
    fprop[i][NY - 1][15] = f[i][NY - 2][13] + 2 * cx[15] * w[15] / RT * u_max;
    fprop[i][NY - 1][16] = f[i][NY - 2][14] + 2 * cx[16] * w[16] / RT * u_max;
    //Layer 3
    fprop[i][NY - 1][20] = f[i][NY - 3][18];
    ///Bottom Wall: 3 Layers are engaged: 0, 1, 2
    //Layer 1
    fprop[i][0][1] = f[i][0][3];
    fprop[i][0][4] = f[i][0][6];
    fprop[i][0][5] = f[i][0][7];

    fprop[i][1][10] = f[i][0][12];
    fprop[i][2][18] = f[i][0][20];
    fprop[i][1][13] = f[i][0][15];
    fprop[i][1][14] = f[i][0][16];
    //
    //Layer 2
    fprop[i][0][10] = f[i][1][12];
    fprop[i][1][18] = f[i][1][20];
    fprop[i][0][13] = f[i][1][15];
    fprop[i][0][14] = f[i][1][16];
    //Layer 3
    fprop[i][0][18] = f[i][2][20];
#endif

#endif //#ifdef couette

#if (defined pois_press || defined pois_PrFr)

#ifdef D2Q9
    //Upper Wall: Only layer 1 is engaged
    fprop[i][NY - 1][3] = f[i][NY - 1][1];
    fprop[i][NY - 1][6] = f[i][NY - 1][4];
    fprop[i][NY - 1][7] = f[i][NY - 1][5];
    //Bottom Wall: Only layer 1 is engaged
    fprop[i][0][1] = f[i][0][3];
    fprop[i][0][4] = f[i][0][6];
    fprop[i][0][5] = f[i][0][7];
#endif

#ifdef D2Q21
    //Upper Wall: 3 Layers are engaged: NY-1, NY-2, NY-3
    //Layer 1
    fprop[i][NY - 1][3] = f[i][NY - 1][1];
    fprop[i][NY - 1][6] = f[i][NY - 1][4]; //(1./6.)*u_max;
    fprop[i][NY - 1][7] = f[i][NY - 1][5]; //(1./6.)*u_max;

    fprop[i][NY - 2][12] = f[i][NY - 1][10];
    fprop[i][NY - 3][20] = f[i][NY - 1][18];
    fprop[i][NY - 2][15] = f[i][NY - 1][13];
    fprop[i][NY - 2][16] = f[i][NY - 1][14];
    //Layer 2
    fprop[i][NY - 1][12] = f[i][NY - 2][10];
    fprop[i][NY - 2][20] = f[i][NY - 2][18];
    fprop[i][NY - 1][15] = f[i][NY - 2][13];
    fprop[i][NY - 1][16] = f[i][NY - 2][14];
    //Layer 3
    fprop[i][NY - 1][20] = f[i][NY - 3][18];
    ///Bottom Wall: 3 Layers are engaged: 0, 1, 2
    //Layer 1
    fprop[i][0][1] = f[i][0][3];
    fprop[i][0][4] = f[i][0][6];
    fprop[i][0][5] = f[i][0][7];

    fprop[i][1][10] = f[i][0][12];
    fprop[i][2][18] = f[i][0][20];
    fprop[i][1][13] = f[i][0][15];
    fprop[i][1][14] = f[i][0][16];
    //
    //Layer 2
    fprop[i][0][10] = f[i][1][12];
    fprop[i][1][18] = f[i][1][20];
    fprop[i][0][13] = f[i][1][15];
    fprop[i][0][14] = f[i][1][16];
    //Layer 3
    fprop[i][0][18] = f[i][2][20];
#endif

#endif //#if (defined pois_press || defined pois_PrFr)

    // Bottom wall to bounce back

  } // Boundary condition (bounce-back)
}

////////////////////////////////////////////////////////////////////
void Output_Write(int check1, int check2, int check3)
{

  if (((t % check1) == 0) || t == 1)
  {
    write_Kolmo(); /*write sequence for plot kolmogorov 
(evolution of results through each WriteItermode sequence)*/
  }
  if ((t % check2) == 0)
    writeLongIteration(); /*write sequence for general output*/

  if ((t % check3) == 0 || t == 1)
    writeEachStep(); /*write sequence for time output, like L2Error-time or decay-time*/

#ifdef diagDecay
  writeEachStepDiag();
#endif
}

////////////////////////////////////////////////////////////////////
void Check_Finish()
{

  b1 = 1;
  if (t == NSTEPS)
  {
    printf("\nReach to the End of Simulation @NSTEPS\n");
    finisher = 1;
    finish_The_Simulation();
  }
}

////////////////////////////////////////////////////////////////////
void U_analyCalculation(int kind)
{
  //kind: 1:dynamic, 2:static
  //calculation of analytical value for Decay kolmogorov
  if (kind == 1)
  { //kind: 1:dynamic
#ifdef decay_kolmg
    for (int i = 0; i < NX; i++)
    {
      u_analx[i] = u_max *
                   exp(-(En * En) * (M_PI * M_PI) * nuo * t / (NX * NX)) *
                   sinl(En * M_PI * (x[i]) / (NX));
    }
    for (int j = 0; j < NY; j++)
    {
      u_analy[j] = u_max2 *
                   exp(-(En2 * En2) * (M_PI * M_PI) * nuo * t / (NY * NY)) *
                   sinl(En2 * M_PI * (y[j]) / (NY));
    }
#endif
//
#ifdef sound_waves
    for (int i = 0; i < NX; i++)
    {
      u_analx[i] = 1.;
    }
    for (int j = 0; j < NY; j++)
    {
      u_analy[j] = 1.;
    }
#endif
//
//calculation of analytical value for forced kolmogorov
#ifdef forced_kolmg
    for (int i = 0; i < NX; i++)
    {
      /*u_analx[i]=u_max*
( 1.-exp(-(En*En)*(M_PI*M_PI)*nuo*t/(NX*NX)) ) *
sinl(En*M_PI*(x[i])/(NX));*/
      u_analx[i] = u_max * sinl(En * M_PI * (x[i]) / (NX)); //umax sin
    }
    for (int j = 0; j < NY; j++)
    {
      //u_analy[j]=u_max2*
      //( 1.-exp(-(En2*En2)*(M_PI*M_PI)*nuo*t/(NY*NY)) )*
      //sinl(En2*M_PI*(y[j])/(NY));
      u_analy[j] = u_max2 * sinl(En2 * M_PI * (y[j]) / (NY));
    }
#endif

#if (defined pois_press || defined pois_PrFr)
    for (int i = 0; i < NY; i++)
    {
      u_analy2[i] = -1. / (2. * nuo * rho[NX - 1][i]) * (gradP) * ((y[i] - ybottom) * (y[i] - ybottom) - (y[i] - ybottom) * ytop);
      u_analy3[i] = -1. / (2. * nuo * rho[NX - 1][i]) * (gradP) * ((y[i] - ybottom) * (y[i] - ybottom) - (y[i] - ybottom) * ytop - ytop * ytop * Kn);
      //from paper "Direct simulation methods for low-speed microchannel flows"
    }
#endif

  } //kind1 Dynamic

  else if (kind == 2) //kind2 Static
  {
    //
    for (int i = 0; i < NX; i++)
    {
#ifdef forced_kolmg
      u_analx[i] = u_max * sinl(En * M_PI * (x[i]) / (NX)); //umax sin
#endif
//
#ifdef decay_kolmg
      u_analx[i] = 0.0; //umax sinl
#endif
    }

    for (int i = 0; i < NY; i++)
    {
#ifdef couette
      u_analy[i] = (u_max / NY) * y[i];
      u_analy2[i] = u_analy[i];
#endif

#if (defined decay_kolmg || defined forced_kolmg)
      u_analy[i] = 0.0;
      u_analy2[i] = u_analy[i];
#endif

#if (defined sound_waves)
      u_analy[i] = 1.0;
      u_analy2[i] = 1.0;
#endif

#if (defined pois_press || defined pois_PrFr)
      u_analy[i] = -4. * u_max / (NY * NY) * (y[i] - ybottom) * (y[i] - ytop);
#endif

    } //geometry setting

    //
  } //kind2 Static
}

////////////////////////////////////////////////////////////////////

void write(int check1)
{
  FILE *fp;

  //"Out_GraphY.dat
  if ((check1 == 1) || (check1 == 4))
  {

    fp = fopen("./output/Out_GraphY.dat", "w");
    char strr[] = "   \"Y\", \"U/Umax\", \"Uanaly/Umax\" , \"Uanaly2/Umax\", \"Uanaly3/Umax\"    ";
    fprintf(fp, "TITLE Iteration= \"%d\" \n", iteration);
    fprintf(fp, "VARIABLES = %s\n", strr);
    fprintf(fp, "ZONE I=%d%s \n", NY, ", F= POINT");
    int kk = NX / 2;
#if (defined pois_press || defined pois_PrFr)
    kk = NX - 1;
    kk = NX / 2;
    fprintf(fp, "#At location: %d, while NX is : %d \n", kk + 1, NX);
#endif
    for (int j = 0; j < NY; j++)
    {
#if (defined decay_kolmg || defined forced_kolmg)
      fprintf(fp, "%Le %Le %Le %Le %Le\n", y[j] / NY, u[kk][j] / u_max, u_analy[j] / u_max, u_analy2[j] / u_max, u_analy3[j] / u_max);
#elif (defined sound_waves)
      fprintf(fp, "%Le %Le\n", y[j] / NY, rho[kk][j]);
#else
      fprintf(fp, "%Le %.10Lf %Le %Le %Le\n", y[j] / NY, u[kk][j] / u_max, u_analy[j] / u_max, u_analy2[j] / u_max, u_analy3[j] / u_max);
#endif
    }
    fclose(fp);
  }

  //"Out_GraphX.dat
  if ((check1 == 2) || (check1 == 4))
  {

    fp = fopen("./output/Out_GraphX.dat", "w");
    fprintf(fp, "TITLE Iteration= \"%d\" \n", iteration);
    char strr2[] = "   \"X\", \"Rho\" , \"U\", \"V\", \"V-analy\"   ";
    fprintf(fp, "VARIABLES = %s\n", strr2);
    fprintf(fp, "ZONE I=%d%s \n", NX, ", F= POINT");

#if (defined pois_press || defined pois_PrFr)
    int a = 1;
    int aa = NX - 1;
#else
    int a = 0;
    int aa = NX;
#endif

    int kk = (NY / 2);
    for (int j = a; j < aa; j++)
    {
#ifdef forced_kolmg
      fprintf(fp, "%Le %Le %Le %Le %Le\n", x[j], rho[j][kk], u[j][kk], v[j][kk], u_analx[j]);
#elif defined(sound_waves)
      int kk = (NY / 2);
      fprintf(fp, "%Le %Le %Le %Le %Le %Le\n", x[j], rho[j][kk], u[j][kk], u[j][kk], u_analy[kk], u_analy2[kk]);
#else
      int kk = (NY / 2);
      fprintf(fp, "%Le %Le %Le %Le %Le %Le\n", x[j], rho[j][kk], u[j][kk], u[j][kk] / u_max, u_analy[kk] / u_max, u_analy2[kk] / u_max);
#endif
    }
    fclose(fp);
  }

  //"Out_Contour.dat"
  if ((check1 == 3) || (check1 == 4))
  {

    fp = fopen("Out_Contour.dat", "w");
    fprintf(fp, "TITLE Iteration= \"%d\" \n", iteration);
    char str[] = "   \"X\", \"Y\",\"Density\", \"U\", \"U-Uanaly\", \"V\"     ";
    fprintf(fp, "VARIABLES = %s\n", str);
    fprintf(fp, "ZONE T = \"%d\" I=%d%s%d%s \n", iteration, NX, ", J= ", NY, ", F= POINT");

    for (int j = 0; j < NY; j++)
    {
      for (int i = 0; i < NX; i++)
      {

        fprintf(fp, "%Le %Le %Le %Le %Le %Le\n", x[i], y[j], rho[i][j], u[i][j], fabsl(u[i][j] - u_analy[j]), v[i][j]);
      }
    }
    fclose(fp); //Contour
  }

  //"Out_GraphXY.dat"
  if (check1 == 5)
  {
    long double Vdiag, loc1;
    int a22;
    fp = fopen("./output/Out_GraphXY.dat", "w");
    char strr11[] = "   \"Y\", \"UV\" ";
    fprintf(fp, "TITLE Iteration= \"%d\" \n", iteration);
    fprintf(fp, "VARIABLES = %s\n", strr11);
    a22 = a11;
    fprintf(fp, "ZONE I=%d%s \n", a22, ", F= POINT");

    for (int j = 0; j < NY; j++)
    {
      for (int i = 0; i < NX; i++)
      {
        if (i == j)
        {
          loc1 = sqrtl(x[i] * x[i] + y[j] * y[j]);
          if (v[i][j] >= 0)
            Vdiag = sqrtl((u[i][j] * u[i][j]) + (v[i][j] * v[i][j]));
          else
            Vdiag = -sqrtl((u[i][j] * u[i][j]) + (v[i][j] * v[i][j]));
          fprintf(fp, "%Le %Le\n", loc1, Vdiag);
        }
      }
    }
    fclose(fp);
  }
}

////////////////////////////////////////////////////////////////////
