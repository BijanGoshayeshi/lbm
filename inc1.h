//******************************************************************************
//Title:
//        Lattice Boltzmann methods (LBM) program for two-dimensional flows
//Version:
//        Version 2.01.03
//History:
//        Written by Bijan Gosaheyshi, 9/2018.
//******************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "inczero.h"

void finish_The_Simulation();
long double **AllocateArray2D(int dim1, int dim2);
long double ***AllocateArray3D(int dim1, int dim2, int dim3);
void write_Kolmo();
void writeLongIteration();
void writeEachStep();
void writeEachStepDiag();
void L2error();
void collision();
void EquilibriumCalculation();
void initialization();
void Macroscopic_Evaluation();
void CheckConvergence();
void BoundaryCheck();
void Stream_and_WallReflection();
void Output_Write();
void Check_Finish();
void U_analyCalculation();
void write();
//------------------------------------------
//select and activate the problem
//#define problm_selection
//#define couette
//#define pois_press
//------------------------------------------
//simulation parameters//'
//-----------------
//#define EquilFunc 1 //  Equlibrium function; 1: Linear, 2: Square
//-----------------
//#define D2Q9
//#define D2Q21
#ifdef D2Q9
#define NPOP 9
#define c_sound (long double)(sqrt(1. / 3.))
#define HigherCoe (long double)(0.0)
#endif
#ifdef D2Q21
#define NPOP 21
#define c_sound (long double)(sqrt(2. / 3.))
#define HigherCoe (long double)(1.0)
#endif

#define RT (long double)(c_sound * c_sound)
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#define scale (long double)1.00 // set simulation size
#define scale_int (int)(scale)
//
//
#ifdef couette
#define NX 20 * scale_int // channel length
#define NY 30 * scale_int // channel width
#define CharLength (long double)(NY)
#define tau (long double)1                              //(0.90)                // relaxation time (BGK model)
#define omega (long double)(1.0 / tau)                  //deltaT/tau
#define u_max (long double)(0.10 / scale)               // maximum velocity
#define u_max2 (long double)(u_max)                     //(0.10/scale)        // maximum velocity
#define nuo (long double)(RT * (2.0 * tau - 1.0) / 2.0) // kinematic shear viscosity  1/6 (2tau-deltaT)
//nuo = deltax2/(deltaT D) (1/omega-1/2)
#define En (int)(2)                                                           //2,4,6,8
#define En2 (int)(2)                                                          //2,4,6,8,16
#define Fkol (long double)(En * En * (M_PI * M_PI) * nuo * u_max / (NX * NX)) //Force kolmogorov amplitude
#define paraiter (long double)(1)
#endif
//
#ifdef pois_press
#define NX 4 * scale_int  // channel length
#define NY 40 * scale_int // channel width
#define CharLength (long double)(NY)
#define tau (long double)1.0 //(sqrt(3./16.)+0.5)    //0.93301270189  relaxation time (BGK model)
#define omega (long double)(1.0 / tau)
#define u_max (long double)0.001    //(0.10/scale)         // maximum velocity
#define u_max2 (long double)(u_max) //(0.10/scale)        // maximum velocity
#define ybottom (long double)(0.0)
#define ytop (long double)(NY)
#define nuo (long double)(RT * (2.0 * tau - 1.0) / 2.0)                       // kinematic shear viscosity
#define En (int)(2)                                                           //2,4,6,8
#define En2 (int)(2)                                                          //2,4,6,8,16
#define Fkol (long double)(En * En * (M_PI * M_PI) * nuo * u_max / (NX * NX)) //Force kolmogorov amplitude
#define paraiter (long double)(2)
#endif
//
#ifdef pois_PrFr
#define NX 3 * scale_int  // channel length
#define NY 88 * scale_int // channel width
#define CharLength (long double)(NY)
#define tau (long double)1.504008657 //(sqrt(3./16.)+0.5)    //0.93301270189  relaxation time (BGK model)
#define omega (long double)(1.0 / tau)
#define u_max (long double)(0.013266499 / scale) // maximum velocity
#define u_max2 (long double)(u_max)              //(0.10/scale)        // maximum velocity
#define ybottom (long double)(0.0)
#define ytop (long double)(NY)
#define nuo (long double)(RT * (2.0 * tau - 1.0) / 2.0)                       // kinematic shear viscosity
#define En (int)(2)                                                           //2,4,6,8
#define En2 (int)(2)                                                          //2,4,6,8,16
#define Fkol (long double)(En * En * (M_PI * M_PI) * nuo * u_max / (NX * NX)) //Force kolmogorov amplitude
#define paraiter (long double)(2)
#endif
//
#if (defined forced_kolmg || defined decay_kolmg)
#define NX 40 * scale_int // channel length
#define NY 40             //3*scale_int                     // channel width
#define CharLength (long double)(NX)
#define tau (long double)0.9      //(sqrt(3./16.)+0.5)    //0.93301270189  relaxation time (BGK model)
#define u_max (long double)0.01   //(0.10/scale)        // maximum velocity
#define u_max2 (long double)0.000 //(0.10/scale)        // maximum velocity
#define dens_max (long double)0.00
#define omega (long double)(1.0 / tau) //// OMEGA ///////////////////
#define ybottom (long double)(0.0)
#define ytop (long double)(NY)
#define nuo (long double)(RT * (2.0 * tau - 1.0) / 2.0) // kinematic shear viscosity
//from paper: Turbulent channel without boundaries: The periodic Kolmogorov flow
#define En (int)(2)                                                           //2,4,6,8,16// L=2*En
#define En2 (int)(2)                                                          //2,4,6,8,16
#define Fkol (long double)(En * En * (M_PI * M_PI) * nuo * u_max / (NX * NX)) //Force kolmogorov amplitude
#define paraiter (long double)(1)
#endif
//
#if (defined sound_waves)
#define NX 64 * scale_int //  wavelength//
#define NY 1              //3*scale_int                     //  width/
#define CharLength (long double)(NX)
#define tau (long double)0.750   //(sqrt(3./16.)+0.5)    //0.93301270189  relaxation time (BGK model)
#define u_max (long double)0.00  //(0.10/scale)        // maximum velocity
#define u_max2 (long double)0.00 //(0.10/scale)        // maximum velocity
#define dens_max (long double)1E-6
#define omega (long double)(1.0 / tau) //// OMEGA /////////////////
#define ybottom (long double)(0.0)
#define ytop (long double)(NY)
#define nuo (long double)(RT * (2.0 * tau - 1.0) / 2.0)                       // shear kinematic shear viscosity
#define En (int)(2)                                                           //2,4,6,8,16// L=2*En// the wave number along x
#define En2 (int)(2)                                                          //2,4,6,8,16//the wave number along y
#define Fkol (long double)(En * En * (M_PI * M_PI) * nuo * u_max / (NX * NX)) //Force kolmogorov amplitude
#define paraiter (long double)(1)
#endif
//

//
#if (defined Doubly_periodic_shear_layers)
#define NX 256 * scale_int //  wavelength//
#define NY 256             //3*scale_int                     //  width/
#define CharLength (long double)(NX)
#define tau (long double)0.50177362      //0.50088681 //(sqrt(3./16.)+0.5)    //0.93301270189  relaxation time (BGK model)
#define u_max (long double)0.023094011   //(0.10/scale)        // maximum velocity
#define u_max2 (long double)0.05 * u_max //(0.10/scale)        // maximum velocity
#define dens_max (long double)1E-6
#define omega (long double)(1.0 / tau) //// OMEGA /////////////////
#define ybottom (long double)(0.0)
#define ytop (long double)(NY)
#define nuo (long double)(RT * (2.0 * tau - 1.0) / 2.0)                       // shear kinematic shear viscosity
#define En (int)(2)                                                           //2,4,6,8,16// L=2*En// the wave number along x
#define En2 (int)(2)                                                          //2,4,6,8,16//the wave number along y
#define Fkol (long double)(En * En * (M_PI * M_PI) * nuo * u_max / (NX * NX)) //Force kolmogorov amplitude
#define paraiter (long double)(1)
#endif
//
///////Test Section////////
//It should be at the end of problem distinctions////
#include "inctest.h"
////////

#define nuPrimeRatio (long double)1 * (2.0 / 3.0) // Default: 1.0*(2.0/3.0)
//From **Dellar** Paper: ratio btw BULK and SHEAR viscosity: @2/3 no BULK effect on equilibrium function
#define nuPrime (long double)(nuPrimeRatio * nuo) //the BULK kinematic viscoity
#define D (long double)(2)                        //the number of spatial dimensions
//
#define Re (long double)(CharLength * u_max / nuo)
#define Kn (long double)((tau - 0.5) * sqrt(M_PI / 2.) * c_sound / CharLength)
//
#ifdef pois_press
#define gradP (long double)(8. * nuo * u_max / (NY * NY))
#define rho_outlet 1.0
#define rho_inlet (long double)((NX - 1.) * gradP / RT + rho_outlet)
#endif
//
#ifdef pois_PrFr
#define Fdensity (long double)(8. * nuo * u_max / (NY * NY))
#define gradP (long double)(0.0) * (8. * nuo * u_max / (NY * NY))
#define rho_outlet 1.0
#define rho_inlet (long double)((NX - 1.) * gradP / RT + rho_outlet)
#endif
//
#define WriteItermode (int)(1 * paraiter) /*write sequence for plot kolmogorov \
(evolution of results through each WriteItermode sequence)*/
//check1//write_Kolmo(); kolxx.dat writer
#define WriteIter (int)(WriteItermode * 25000)
//check for a value from testMode
#ifdef testModeOne
#undef WriteIter
#define WriteIter (int)(T_WriteIter)
#endif
/*kolmogorov up limit writer, write each WriteItermode until reaching WriteIter*/
#define NSTEPS (long)(1e0 * WriteIter)
// number of simulation time steps
#define eachstep_interval (int)(WriteIter / 10)
//check for a value from testMode
#ifdef testModeOne
#undef eachstep_interval
#define eachstep_interval (int)(T_eachstep_interval)
#endif
/*write sequence for general output*/
//check3//writeEachStep();
#define output_interval (int)(1 * eachstep_interval)
/*write sequence for general output in screen*/
//check2//writeLongIteration();
#define teval (int)(200)
// tolerance to steady state convergence
#define tol (long double)(1e-12)

long double a11, b11;
long double err_iter;
long double ZetaU;
long double ZetaF;
long double UF;
long double UTwo;
long double ZetaTwo;
long double FourthTerm;
long double Cs2Calculate;
long double Zigma11;
long double Zigma12;
long double Zigma21;
long double Zigma22;

long double HermTWO11;
long double HermTWO12;
long double HermTWO21;
long double HermTWO22;
long double L2;
long double L2Y;

long double TrPiConst;

int newx;
int newy;
int iteration;
int finisher;
int finisher2;
int b1;
int conv_counter;
int L2errorwriter;
int L2_counter;
int t;

long double *w;
long double *cx;
long double *cy;
long double *x;       //long double x[NX];
long double *y;       //long double y[NY];
long double *u_analy; //long double u_analy[NY];
long double *u_analx;
long double *u_analy2;   //long double u_analy[NY];
long double *u_analy3;   //long double u_analy[NY];
long double ***feq;      //long double feq[NX][NY][NPOP];
long double ***feqP;     //long double feq[NX][NY][NPOP];
long double ***f;        //long double f[NX][NY][NPOP];
long double ***fBar;     //long double f[NX][NY][NPOP];
long double ***fBarprop; //long double f[NX][NY][NPOP];
long double ***fprop;    //long double fprop[NX][NY][NPOP];
long double ***Force;
long double **u_old;     //long double u_old[NX][NY];
long double **rho;       //long double rho[NX][NY];
long double **u;         //long double u[NX][NY];
long double **v;         //long double v[NX][NY];
long double **vorticity; //long double vorticity[NX][NY];
long double **fx;        //force along x
long double **fy;        //force along y
long double **TrPiBar;
long double **TrPiOne;
long double **DivU;

long double *error; //long double error[NX];
long double conv;
long double mean1;
long double mean2;

///////////////////////////////////////////////////
#ifndef _INC1_H

#define _INC1_H

#endif /* _INC1_H */
///////////////////////////////////////////////////
