set title "Length vs. Uvel"
set ylabel "Uvel"
set xlabel "Length-X"
plot "Kol1.dat" using 1:2 title '1' with lines,\
"Kol2.dat" using 1:2 title '2',\
"Kol3.dat" using 1:2 title '3'
pause -1 "Hit any key to continue"