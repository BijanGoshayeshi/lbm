set title "Length vs. Uvel"
set ylabel "Uvel"
set xlabel "Length X"
plot "Out_GraphX.dat" using 1:3 title 'LBM output' with lines,
pause -1 "Hit any key to continue"

set title "Length vs. Vvel"
set ylabel "Vvel"
set xlabel "Length X"
plot "Out_GraphX.dat" using 1:4 title 'LBM output' with lines,\
"Out_GraphX.dat" using 1:5 title 'ANALYTICAL'
pause -1 "Hit any key to continue"

