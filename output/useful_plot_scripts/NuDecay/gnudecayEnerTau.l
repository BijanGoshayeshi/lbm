
reset
set terminal postscript eps enhanced color font 'Helvetica,20'
set output 'RateEnergyVsTau.eps'
set title "Energy-Decay-Rate vs. {/Symbol t}"
set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"
set xlabel "{/Symbol t}"
set ylabel "Energy-Decay-Rate"
unset key
plot "decayEnergyvsTau.dat" using 1:2 with lp

