reset
set terminal postscript eps enhanced color font 'Helvetica,20'
set output 'decayIterlogEnergy.eps'
set fit logfile "fit-dataEnergy.log"
f(x) = a + b*x
fit f(x) '../writeEachStep2.dat' using ($3):(log($10)) via a,b;

set title "Energy vs. time"
#set logscale x
set logscale y
set ylabel "Energy"
set xlabel "time"
set key font ",14"
plot "../writeEachStep2.dat" using 3:10 title 'LBM 2 {/Symbol p}/L' with lp

reset
set terminal postscript eps enhanced color font 'Helvetica,20'
set output 'decayIterlogVelocity.eps'
set fit logfile "fit-dataVelcoity.log"
f(x) = a + b*x
fit f(x) '../writeEachStep2.dat' using ($3):(log($2)) via a,b;

set title "Velocity vs. time"
#set logscale x
set logscale y
set ylabel "Velocity"
set xlabel "time"
set key font ",14"
plot "../writeEachStep2.dat" using 3:2 title 'LBM 2 {/Symbol p}/L' with lp

reset
set terminal postscript eps enhanced color font 'Helvetica,20'
set output 'decayIterlogDensityPerturb.eps'
set fit logfile "fit-dataDensityPerturb,log"
f(x) = a + b*x
fit f(x) '../writeEachStep2.dat' using ($3):(log($1)) via a,b;

set title "DensityPerturb vs. time"
#set logscale x
set logscale y
set ylabel "DensityPerturb"
set xlabel "time"
set key font ",14"
plot "../writeEachStep2.dat" using 3:2 title 'LBM 2 {/Symbol p}/L' with lp








