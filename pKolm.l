set title "Length vs. Vvel" 
set ylabel "Vvel" 
set xlabel "Length-X" 
plot "Kol1.dat" using 1:2 title '1' with lines,"Kol200.dat" using 1:2 title '200',"Kol400.dat" using 1:2 title '400',"Kol600.dat" using 1:2 title '600',"Kol800.dat" using 1:2 title '800',"Kol1000.dat" using 1:2 title '1000' 
pause -1000 "Hit any key to continue" 