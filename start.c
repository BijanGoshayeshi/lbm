//******************************************************************************
//Title:
//        Lattice Boltzmann methods (LBM) program for two-dimensional flows
//Version:
//        Version 2.01.03
//History:
//        Written by Bijan Gosaheyshi, 9/2018.
//******************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int TNX, TNY, testMode, TRunNumber, T_WriteIter, T_eachstep_interval;
long double Ttau, TU_max, TU_max2, Tdens_max;
long double TauTest = 0;
int EquilFunc;
int Ptoblem_Selec;
int Vel_set_selection;
int Dell_Visc_Actvn;
int main()
{
  FILE *fpm;
  FILE *ffpm;
  fpm = fopen("pre-program.txt", "r");
  ffpm = fopen("pre-program_Selction_Explained0.txt", "w");
  fscanf(fpm, "%d\n", &testMode);
  fscanf(fpm, "%d", &EquilFunc);
  fscanf(fpm, "%d", &Ptoblem_Selec);
  fscanf(fpm, "%d", &Vel_set_selection);
  fscanf(fpm, "%d", &Dell_Visc_Actvn);
  fclose(fpm);

  FILE *fp;
  fp = fopen("inczero.h", "w");

  if (testMode == 1)
  {
    printf("testMode is Activated\n");
    fprintf(ffpm, "testMode is Activated\n");
    fprintf(fp, "#define testModeOne\n");
  }

  if (EquilFunc == 1)
  {
    printf("Eqilibrium function is linear %d \n", EquilFunc);
    fprintf(ffpm, "Eqilibrium function is linear \n");

    fprintf(fp, "#define EquilFuncL\n");
  }
  else if (EquilFunc == 2)
  {
    printf("Eqilibrium function is Square %d \n", EquilFunc);
    fprintf(ffpm, "Eqilibrium function is Square\n");
    fprintf(fp, "#define EquilFuncS\n");
  }

  if (Ptoblem_Selec == 1)
  {
    printf("The couette problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The couette problem is being simulated \n");
    fprintf(fp, "#define couette\n");
  }
  else if (Ptoblem_Selec == 2)
  {
    printf("The GradP_poiseuille problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The GradP_poiseuille problem is being simulated\n");
    fprintf(fp, "#define pois_press\n");
  }
  else if (Ptoblem_Selec == 3)
  {
    printf("The GradP_LinearForce_poiseuille problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The GradP_LinearForce_poiseuille problem is being simulated\n");
    fprintf(fp, "#define pois_PrFr\n");
  }
  else if (Ptoblem_Selec == 4)
  {
    printf("The decay-kolmogorov problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The decay-kolmogorov problem is being simulated\n");
    fprintf(fp, "#define decay_kolmg\n");
  }
  else if (Ptoblem_Selec == 5)
  {
    printf("The forced-kolmgorov problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The forced-kolmgorov problem is being simulated\n");
    fprintf(fp, "#define forced_kolmg\n");
  }
  else if (Ptoblem_Selec == 6)
  {
    printf("The decay-kolmgorov diagonal problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The decay-kolmgorov diagonal problem is being simulated\n");
    fprintf(fp, "#define decay_kolmg\n");
    fprintf(fp, "#define diagDecay\n");
  }
  else if (Ptoblem_Selec == 7)
  {
    printf("The sound-wave problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The sound-wave problem is being simulated\n");
    fprintf(fp, "#define sound_waves\n");
  }
  else if (Ptoblem_Selec == 8)
  {
    printf("The Doubly-periodic-shear-layers problem is being simulated %d \n", Ptoblem_Selec);
    fprintf(ffpm, "The Doubly-periodic-shear-layers problem is being simulated\n");
    fprintf(fp, "#define Doubly_periodic_shear_layers\n");
  }

  if (Vel_set_selection == 1)
  {
    printf("D2Q9 velocity set is selected %d \n", Vel_set_selection);
    fprintf(ffpm, "D2Q9 velocity set is selected \n");

    fprintf(fp, "#define D2Q9\n");
  }
  else if (Vel_set_selection == 2)
  {
    printf("D2Q21 velocity set is selected %d \n", Vel_set_selection);
    fprintf(ffpm, "D2Q21 velocity set is selected\n");
    fprintf(fp, "#define D2Q21\n");
  }

  if (Dell_Visc_Actvn == 0)
  {
    printf("Dellar Equilibrium correction is INactivated %d \n", Dell_Visc_Actvn);
    fprintf(ffpm, "Dellar Equilibrium correction is INactivated \n");

    fprintf(fp, "#define DELLAR_ZERO\n");
  }
  else if (Dell_Visc_Actvn == 1)
  {
    printf("Dellar Equilibrium correction is Activated %d \n", Dell_Visc_Actvn);
    fprintf(ffpm, "Dellar Equilibrium correction is Activated\n");
    fprintf(fp, "#define DELLAR_ONE\n");
  }

  fclose(fp);
  fclose(ffpm);
  /////////inctest.h////////
  fp = fopen("./TestFolder/TestData.txt", "r");

  fscanf(fp, "%d\n", &TRunNumber);
  fscanf(fp, "%d\n", &TNX);
  fscanf(fp, "%d\n", &TNY);
  fscanf(fp, "%Le\n", &Ttau);
  fscanf(fp, "%Le\n", &TU_max);
  fscanf(fp, "%Le\n", &TU_max2);
  fscanf(fp, "%Le\n", &Tdens_max);
  fscanf(fp, "%d\n", &T_WriteIter);
  fscanf(fp, "%d\n", &T_eachstep_interval);

  fclose(fp);
  printf("Test Run Number = %d\n", TRunNumber);
  printf("TNX,TNY,Ttau,TU_max,TU_max2,Tdens_max = %d,%d,%Le,%Le,%Le,%Le\n", TNX, TNY, Ttau, TU_max, TU_max2, Tdens_max);
  printf("Time_Steps: %d, write interval: %d\n", T_WriteIter, T_eachstep_interval);
  //////
  ///////
  ////////
  fp = fopen("inctest.h", "w");

  fprintf(fp, "#if (defined testModeOne)\n");
  //first #undef previous ones
  fprintf(fp, "#define TRunNumber %d\n", TRunNumber);
  fprintf(fp, "#undef NX\n");
  fprintf(fp, "#undef NY\n");
  fprintf(fp, "#undef tau\n");
  fprintf(fp, "#undef u_max\n");
  fprintf(fp, "#undef u_max2\n");
  fprintf(fp, "#undef dens_max\n");

  fprintf(fp, "\n");
  //
  fprintf(fp, "#define NX %d\n", TNX);
  fprintf(fp, "#define NY %d\n", TNY);
  fprintf(fp, "#define tau (long double)  %Le\n", Ttau);
  fprintf(fp, "#define u_max (long double)  %Le\n", TU_max);
  fprintf(fp, "#define u_max2 (long double)  %Le\n", TU_max2);
  fprintf(fp, "#define dens_max (long double)  %Le\n", Tdens_max);

  fprintf(fp, "\n");
  //for other definitions//
  //first undfine
  fprintf(fp, "#undef CharLength\n");
  fprintf(fp, "#undef omega\n");
  fprintf(fp, "#undef ybottom\n");
  fprintf(fp, "#undef ytop\n");
  fprintf(fp, "#undef nuo\n");
  fprintf(fp, "#undef En\n");
  fprintf(fp, "#undef En2\n");
  fprintf(fp, "#undef Fkol\n");
  fprintf(fp, "#undef paraiter\n");
  //second define
  fprintf(fp, "#if (defined pois_PrFr)\n");
  fprintf(fp, "#define CharLength (long double) (NY) \n");
  fprintf(fp, "#else\n");
  fprintf(fp, "#define CharLength (long double) (NX) \n");
  fprintf(fp, "#endif\n");

  fprintf(fp, "#define omega (long double) (1.0/tau) \n");
  fprintf(fp, "#define ybottom (long double) (0.0)\n");
  fprintf(fp, "#define ytop (long double) (NY)\n");
  fprintf(fp, "#define nuo (long double) (RT*(2.0*tau-1.0)/2.0) \n");
  fprintf(fp, "#define En (int) (2)\n");
  fprintf(fp, "#define En2 (int) (2)\n");
  fprintf(fp, "#define Fkol (long double) (En*En*(M_PI*M_PI)*nuo*u_max/(NX*NX)) \n");
  fprintf(fp, "#define paraiter (long double) (1)\n");
  fprintf(fp, "#define T_WriteIter (int) %d\n", T_WriteIter);
  fprintf(fp, "#define T_eachstep_interval (int) %d\n", T_eachstep_interval);
  //for other definitions//

  fprintf(fp, "#endif\n");

  /////////inctest.h////////

  return 0;
}