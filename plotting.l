set title "Height vs. Velociy"
set xlabel "Velocity"
set ylabel "Height"
plot "Out_GraphY.dat" using 2:1 title 'LBM output' with lines,\
"Out_GraphY.dat" using 3:1 title 'Analtical'
pause -1 "Hit any key to continue"
set title "Length vs. density"
set ylabel "density"
set xlabel "Length X"
plot "Out_GraphX.dat" using 1:2 title 'LBM output' with lines,
pause -1 "Hit any key to continue"