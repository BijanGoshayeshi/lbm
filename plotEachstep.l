set title "V-vel vs. iteration"
set xlabel "Iteration"
set ylabel "V-vel"
set xrange [0:3500]
plot "writeEachStep.dat" using 3:1 title 'LBM output' with lines, "writeEachStep.dat" using 3:2 title 'Analitical output' with lines
pause -1 "Hit any key to continue"
